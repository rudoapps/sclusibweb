<?php
	session_start();
	include_once("../magic/define.php");
	include_once("../magic/config.php");
	include_once("../magic/class.DB.php");
	$DB = new DB(_SERVER,_USERNAME,_PASSWORD,_DB);
	if(isset($_GET['code'])){
		echo $_GET['code'];
		$url = "https://api.instagram.com/oauth/access_token";
	    $access_token_parameters = array(
	        'client_id'                =>     '3ff0c1d8215d4256b9c683a995e079fd',
	        'client_secret'            =>     '4c1632a35a584b9b9ffe37d7285574d8',
	        'grant_type'               =>     'authorization_code',
	        'redirect_uri'             =>     'http://www.sclusib.com/talent/?go=instagram',
	        'code'                     =>     $_GET['code']
	    );

		$curl = curl_init($url);    // we init curl by passing the url
	    curl_setopt($curl,CURLOPT_POST,true);   // to send a POST request
	    curl_setopt($curl,CURLOPT_POSTFIELDS,$access_token_parameters);   // indicate the data to send
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   // to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // to stop cURL from verifying the peer's certificate.
	    $result = curl_exec($curl);   // to perform the curl session
	    curl_close($curl);   // to close the curl session

	    $arr = json_decode($result,true);
	    //Mostrariamos los resultados de la consulta
	    //print_r($arr);
	    
	    $url = "https://api.instagram.com/v1/users/self/?access_token=".$arr["access_token"];
	    $curl = curl_init($url);    // we init curl by passing the url
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   // to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // to stop cURL from verifying the peer's certificate.
	    $result = curl_exec($curl);   // to perform the curl session
	    curl_close($curl);   // to close the curl session
		$arr = json_decode($result);
	    
	    //Obtenemos el perfil del usuario
	    //print_r($arr);
	    /*
 		Object ( 
 			[meta] => stdClass Object ( [code] => 200 ) 
 			[data] => stdClass Object ( 
 										[username] => desarrollosc 
 										[bio] => Desarrollando Sc 
 										[website] => 
 										[profile_picture] => 'https://scontent.cdninstagram.com/t51.2885-19/11906329_960233084022564_1448528159_a.jpg'
 										[full_name] => Desarrollo sc 
 										[counts] => stdClass Object ( [media] => 0 [followed_by] => 2 [follows] => 2 ) [id] => 1961327828 ) ) 
		*/

 		$DB->open();	
		
		//Si queremos saber si esta registrado el usuario descomentar esto
		$sql = "SELECT * FROM `sb_influencer_register` WHERE id_instagram='".$arr->data->id."'";
		if (!$result = $DB->query($sql)) { 			
    		$DB->error();
    		$DB->close();
    		$ok=false;
		}else{			
			if($result->num_rows>0){
				//UPDATE						
				$sql = "UPDATE `sb_influencer_register` SET 													 	
														`username`  = '".$arr->data->username."',
														`followed_by` = '".$arr->data->counts->followed_by."',
														`follows` = '".$arr->data->counts->follows."',
														`media` = '".$arr->data->counts->media."',
														`profile_picture` = '".$arr->data->profile_picture."',
														`fullname` = '".$arr->data->full_name."',
														`bio` = '".$arr->data->bio."',
														`website` = '".$arr->data->website."',
														`update_at` = CURRENT_TIMESTAMP		
												    WHERE `id_instagram` ='".$_SESSION['info']->data->id."'";
				
				if (!$result = $DB->query($sql)) { 			
		    		$DB->error();
		    		$DB->close();
		    		$ok=false;
				}else{
					$DB->close();
					$ok=true;
				}
			}else{
				//NUEVO
						$sql = "INSERT INTO `sb_influencer_register` (`id_instagram`, 
														`username`,
														`followed_by`,
														`follows`,
														`media`,
														`profile_picture`,
														`fullname`,
														`bio`,
														`website`,
														`created_at`
														) 
									  VALUE ('".$arr->data->id."',
									  		 '".$arr->data->username."',
										     '".$arr->data->counts->followed_by."',
										     '".$arr->data->counts->follows."',
										   	 '".$arr->data->counts->media."',
										   	 '".$arr->data->profile_picture."',
										   	 '".$arr->data->full_name."',
										     '".$arr->data->bio."',
										     '".$arr->data->website."',
										     CURRENT_TIMESTAMP
										     )";										    
		
						if (!$result = $DB->query($sql)) { 			
				    		$DB->error();
				    		$DB->close();   
				    		$ok=false; 		
						}else{
							$DB->close();							
							$ok=true;
						}
			}
			if($ok==true){
				$_SESSION["info"]=$arr;
				if($arr->data->counts->followed_by>=10000){
				   	header('Location: '._URL."unete/top");
				}else{
				   	header('Location: '._URL."unete/usuario");
				}
			}
		}		
		$DB->close();
		//echo $arr->data->counts->followed_by;
 		
	    
	}
	if(isset($_GET['error'])){
		echo $_GET['error'].'<br/>';
		echo $_GET['error_reason'].'<br/>';
		echo $_GET['error_description'].'<br/>';
	}
	
?>