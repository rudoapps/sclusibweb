			<div class="col-xs-6 col-sm-3 cardInfluencer">			
				<div class="bio">
					<div class="round" style="background-image: url({url}img/profile/{id}.jpg)">
	        			
	        		</div>
	
					<h3><a href="/bio/{url_profile}">{name}</a></h3>
					<p class="roman size12 left box">
						{text}
					</p>
					<div class="box-link size12 roman">
						<a href="/bio/{url_profile}" >Ver más...</a>
					</div>
					<div class="box-social">
						<div class="row">
							<div class="col-md-12 center">
								<a href="https://www.instagram.com/{instagram}/" target="_blank"><img src="{url}img/social/ic_instagram.png">	</a>					
								<a href="https://twitter.com/{twitter}" target="_blank"><img src="{url}img/social/ic_twitter.png"></a>
							</div>						
						</div>
					</div>
				</div>
			</div>