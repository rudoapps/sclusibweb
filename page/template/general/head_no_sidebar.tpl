<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>sclusib</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="¿Eres el fan número 1? Sigue a tus instagramers, tuitstar y youtubers favoritos y demuestra lo que te gustan." />
{meta}
<!-- Bootstrap -->
<link rel="stylesheet" href="{url}bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{url}bootstrap/css/bootstrap-theme.min.css">
<script src="{url}bootstrap/js/bootstrap.min.js"></script>
<!-- CSS -->
<link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{url}css/popup.css">
<link rel="stylesheet" href="{url}css/main.css">
<link rel="stylesheet" href="{url}css/small.css">
<link rel="stylesheet" href="{url}css/smaller.css">
<!-- FAVICON -->
<link rel="shortcut icon" type='image/x-icon' href="{url}favicon.ico"/>
<script src="{url}js/jquery-2.2.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Hotjar Tracking Code for http://www.sclusib.com -->
<!--<script type="text/javascript" src="//code.jquery.com/jquery-compat-git.js"></script>-->
<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-61907046-1', 'auto');
	  ga('send', 'pageview');

	</script>
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:207035,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<script>
/**
* Función de Analytics que realiza un seguimiento de un clic en un enlace externo.
* Esta función toma una cadena de URL válida como argumento y la utiliza
* como la etiqueta del evento. Configurar el método de transporte como "beacon" permite que el hit se envíe
* con "navigator.sendBeacon" en el navegador que lo admita.
*/
var trackOutboundLink = function(url) {
   ga('send', 'event', 'outbound', 'click', url, {
     'transport': 'beacon',
     'hitCallback': function(){document.location = url;}
   });
}
</script>
<script type="text/javascript">
	var change=0;
	function cycleImages(){
	    var $active = $('#cycler .active');
	    var $next = ($active.next().length > 0) ? $active.next() : $('#cycler img:first');
	    $next.css('z-index',2);//move the next image up the pile
	    $active.fadeOut(1500,function(){//fade out the top image
		$active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
	        $next.css('z-index',3).addClass('active');//make the next image the top one
	    });
    }

	$(document).ready(function(){	
		$( ".add" ).click(function() {
			$(".new-inf").fadeToggle();
		});
		$( ".change" ).click(function() {
			//alert(">>" + $(this).parent().find(".username").text());
			var data 	 = $(this).parent().parent().parent().find('img').attr("src");
			var username = $(this).parent().parent().find(".username").text();
			var id 		 = $(this).parent().parent().parent().find(".selectorID").val();
			var pos 	 = $(this).parent().parent().parent().find(".selectorPOS").val();
			var desc 	 = $(this).parent().parent().find(".desc").text();
				
			$(".img-desc img").attr("src",data);			
			$(".new-desc").find(".usernametop").html('');
			$(".new-desc").find(".usernametop").html(username);
			$(".new-desc").find(".itemID").val(id);
			$(".new-desc").find(".newDesc").val(desc);
			$(".new-desc").find(".itemPOS").val(pos);
			$(".new-desc").fadeToggle();
		});  
		$( "#accept-desc" ).click(function() {
			$( "#newDesc" ).submit();
		});
		$( "#accept" ).click(function() {
			$( "#newInfluencer" ).submit();
		}); 
		$( ".accept-list" ).click(function() {
			if(change==1){
				$(".new-list").fadeToggle();
				//$( "#newList" ).submit();
			}else{
				alert("No hay cambios");
			}
		}); 
		$(".accept-newlist").click(function(){
			$( "#newList" ).submit();
		});
		$('.delete').click(function(){
		    return confirm("Estas seguro que quieres borrar el registro?");
		});

		setInterval('cycleImages()', 5000);
	
	    $('.view-profile').on('click', function(event){
	        event.preventDefault();
	        $('.cd-popup').addClass('is-visible');
	    });
	    	    
	    var open = 0;
	    $("#menu-toggle-btn").click(function() {
			$(this).toggleClass("open");
			$(".nav-pills").toggle();
		});
		$("#see_all").click(function() {			
			$("#more_influencers").toggle();
			$("#see_all").hide();
		});
		$('.admin-select').on('change', function(event){
              if($(this).val()!='0'){
              var url = '{url-website}admin/'+$(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url; // redirect
                }
              }
                return false;
          });
  
          $( "#accept" ).click(function() {
              $( "#profile" ).submit();
          });
  
          $( "#add" ).click(function() {
              var url = '{url-website}admin/';
              window.location = url;
          });
  
          $( "#preview" ).click(function() {
              var url = '{url-website}talent/{url_profile}';
              window.location = url;
          });
  
          $("#delete").click(function(){
              var txt;
              var r = confirm("¿Confirmas que deseas borrar el perfil?");
              if (r == true) {
                  var url = '{url-website}wand/{url_profile}';
                  window.location = url;
              } 
          });    
          $(".unete").click(function(){
          		    $('html,body').animate({
				        scrollTop: $("#entrar").offset().top},
				        'slow');
		  });
		$(".join_sclusib").hover(function(){
			$(this).css('opacity', '0.6');
		});	
		$(".join_sclusib").mouseout(function(){
			$(this).css('opacity', '1');
		});	  
		
          $(".join_sclusib").click(function(){
				$(this).css('margin-top', '3px');
          		var url= 'https://api.instagram.com/oauth/authorize/?client_id=3ff0c1d8215d4256b9c683a995e079fd&redirect_uri=http://www.sclusib.com/talent/?go=instagram&response_type=code';
          		window.location = url;
          });
          $( ".usuario" ).click(function() {
              $( "#usuario" ).submit();
          });
          $( ".top_influencer" ).click(function() {          	
          	if($('#top_influencer_check').is(":checked")){
              	$( "#top_influencer" ).submit();
            }else{
            	alert("Tienes que aceptar el acuerdo de partner antes de inscribirte");
            }
          });
	});

	$(function() {
		var i=0;
	    $( "#sortable" ).sortable();

	    $( "#sortable" ).disableSelection();    
	    $('ul#sortable').sortable({
	        start: function(event, ui) {
	            var start_pos = ui.item.index();            
	            ui.item.data('start_pos', start_pos);
	        },
	        update: function(event, ui) {
	            var start_pos = ui.item.data('start_pos');
				var end_pos = ui.item.index();	            
	            
	            change = 1;
	            $('#sortable li').each( function(e) {
	              i++;
	              //alert("ANTES: "+$(this).find('.selectorID').val()+ " " +$(this).find('.selectorPOS').val()+ " > index:"+$(this).index());
	              $(this).find('.selectorPOS').val($(this).index());
	              //alert("AHORA: "+$(this).find('.selectorID').val()+ " " +$(this).find('.selectorPOS').val()+ " > index:"+$(this).index());
	              //order.push( $(this).attr('id')  + '=' + ( $(this).index() + 1 ) );
	            });
	            
	            //alert(start_pos + ' - ' + end_pos);
	            //alert(ui.item.find('.selectorPOS').val());
	            //ui.item.find('.selectorPOS').val(end_pos);
	            //alert(ui.item.find('.selectorPOS').val());
	        }
	    });
	});

	$(function(){
        //prepare Your data array with img urls
        
        var dataArray=new Array();
        dataArray[0]="http://www.sclusib.com/img/ranking/ad.png";
        dataArray[1]="http://www.sclusib.com/img/ranking/ad_next.png";

        //start with id=0 after 5 seconds
        var thisId=0;
/*
        
            $('.admockup').attr('src',dataArray[thisId]);
            thisId++; //increment data array id
            if (thisId==2) thisId=0; //repeat from start
        },3000);   */
        window.setInterval(function(){
        $('.admockup').fadeOut(function () {
         	$('.admockup').attr('src', dataArray[thisId]);
         	thisId++;
         	if (thisId == 2) thisId = 0;
         	$('.admockup').fadeIn(); 
        });  
        },3000);   
    });

</script>
</head>