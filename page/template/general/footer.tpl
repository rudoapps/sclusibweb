<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="footer-social">
					<a href="https://twitter.com/sclusib" target="blank"><img src="http://www.sclusib.com/wp-content/themes/sclusib/img/twitter_round.png"></a>
        			<a href="https://www.facebook.com/sclusib/" target="blank"><img src="http://www.sclusib.com/wp-content/themes/sclusib/img/facebook_round.png"></a>
        			<a href="https://www.instagram.com/sclusib/" target="blank"><img src="http://www.sclusib.com/wp-content/themes/sclusib/img/instagram_round.png"></a>
        			<!--<img src="<?php bloginfo('template_directory');?>/img/rss_round.png">-->
				</div>
				<img src="http://www.sclusib.com/wp-content/themes/sclusib/img/sclusib_logo_blanco.png" class="footer-logo">
				 
				<p>© Sclusib Studio S.L.<br/>
				C/Sorni 7 - 46004 - Valencia. <a href="mailto:hola@sclusib.com">hola@sclusib.com</a></p>
				
				<p>
				<a href="http://www.sclusib.com/quienes-somos/">Quiénes somos</a><br/>
				<a href="http://www.sclusib.com/contacto/">Contacta con nosotros</a><br/>
				<a href="http://www.sclusib.com/anunciate/">Anúnciate</a></p>
			</div>
		</div>
	</div>
</div>

	
