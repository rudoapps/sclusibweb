<h1>Sclusib. Términos y condiciones.</h1>

<p>
<b>26 de octubre de 2015.</b><br/>
Al utilizar o acceder a los servicios de sclusib, muestras tu conformidad con estos términos y condiciones, que se actualizan periódicamente según se estipula en la sección “actualización”.
</p>
<p>
Debes ser mayor de 13 años para utilizar sclusib. Sclusib no recopila ni solicita premeditadamente información de cualquier persona menor de 13 años ni permite a esas personas registrarse en el servicio. El servicio y su contenido no están dirigidos a niños menores de 13 años. En el caso de que nos enteremos de que hemos recopilado información personal de un niño menor de 13 años sin el consentimiento paterno, borraremos esa información tan pronto como fuera posible. Si crees que podemos tener algún tipo de información acerca de un niño menor de 13 años, por favor ponte en contacto con nosotros en <a href="mailto:soporte@sclusib.com">soporte@sclusib.com</a>.
</p>
<h2>
Contenidos
</h2>
<p>
No puedes publicar fotos u otro tipo de contenido que muestre imágenes violentas, de desnudos íntegros o parciales, discriminatorias, ilegales, transgresoras, ofensivas, hirientes, intimidatorias, pornográficas o con contenido sexual a través del servicio.
</p>
<p>
Manifiestas y garantizas que: (i) eres propietario del Contenido que publicas en el servicio o a través de este o que, de otro modo, tienes el derecho de otorgar los derechos y las licencias estipulados en estas condiciones de uso; (ii) la publicación y utilización de tu contenido en el servicio o a través de este no infringe, malversa ni viola los derechos de terceros, incluidos sin limitación los derechos de privacidad, de publicidad, de autor, de marcas comerciales y/u otros derechos de propiedad intelectual; (iii) aceptas pagar todos los derechos de autor, las tasas y cualquier otra suma adeudada como consecuencia del Contenido publicado en el servicio o a través de este; y (iv) tienes el derecho y la capacidad legales de cumplir estas condiciones de uso en tu jurisdicción.
Eres el propietario de todo el contenido y la información que publicas en sclusib. En relación con el contenido con derechos de propiedad intelectual (contenido de PI), como fotos y vídeos, nos otorgas específicamente una licencia no exclusiva, transferible, con posibilidad de ser subotorgada, libre de regalías y aplicable globalmente para utilizar cualquier contenido de PI que publiques en sclusib.
Uso
</p>

<p>
No puedes usar sclusib para ningún propósito ilegal, no autorizado o que viole los derechos de otros. Te comprometes a cumpliar con las leyes aplicables en tu país, estado y provincia.
No puedes difamar, acosar, amenazar, intimidar o suplantar a otros usuarios de sclusib.
No puedes divulgar información personal o privada de otros usuarios.
Te comprometes a no vender, transferir, otorgar licencias o ceder tu cuenta, seguidores, nombre de usuario ni los derechos de tu cuenta.
Aceptas no solicitar, recopilar o utilizar las credenciales de inicio de sesión de otros usuarios.
</p>
<h2>
Responsabilidad
</h2>
<p>
Eres el único responsable de tu conducta y de cualquier contenido (dato, texto, información, gráfico, foto, perfil, clip de audio y vídeo, enlace) o actividad que publiques, envíes, muestres a través de sclusib o que ocurra bajo tu cuenta de usuario.
</p>
<p>
La información que proporcionas en el registro y más adelante en tu foto de perfil, tus fotos o tus comentarios es tu responsabilidad.
</p>
<p>
Eres el único responsable de la interacción que establezcas con otros usuarios del servicio, ya sea con o sin conexión. Aceptas que sclusib no es responsable de la conducta de los usuarios. Aunque no está obligado, sclusib se reserva el derecho de supervisar o involucrarse en los conflictos que mantengas con otros usuarios. Actúa con sentido común y utiliza tu mejor criterio al interactuar con otros usuarios, incluso cuando envíes o publiques contenido o cualquier tipo de información personal o de otro tipo.
</p>
<h2>
Seguridad
</h2>
<p>
Aunque proporcionamos normas para la conducta de los usuarios, no controlamos ni dirigimos sus acciones en sclusib y no somos responsables del contenido o la información que los usuarios transmitan o compartan. No somos responsables de ningún contenido que se considere ofensivo, inapropiado, obsceno, ilegal o inaceptable que puedas encontrar en sclusib. No nos hacemos responsables de la conducta de ningún usuario de sclusib, ya sea en internet o en otros medios.
</p>
<p>
Aunque no estamos obligados, podemos eliminar, editar, bloquear y/o supervisar el Contenido o las cuentas que incluyan Contenido, siempre que determinemos, a nuestra entera discreción, que infringen estas condiciones de uso.
</p><p>
Usamos moderadores para señalar y eliminar contenidos ofensivo así como a los usuarios que los generan. Del mismo modo puedes reportar cualquier tipo de contenido ofensivo así como a los usuarios que los generan. En un máximo de 24 horas eliminaremos ese contenido y podremos cerrar la cuenta del usuario que lo haya generado.
</p>
<p>
La infracción de estas condiciones de uso puede dar como resultado el cierre de tu cuenta de sclusib, lo cual queda a la entera discreción de sclusib. Entiendes y aceptas que sclusib no puede ni se hará responsable del Contenido publicado en el servicio y utilizas el servicio bajo tu propia responsabilidad. Si infringes la esencia o el espíritu de estas condiciones de uso, o de algún otro modo provocas el riesgo de que seamos expuestos legalmente, podríamos dejar de proporcionarte todo el servicio o parte de él.
</p>
<h2>
Acceso
</h2>
<p>
Nos reservamos el derecho de no permitir el acceso al servicio a cualquier persona, por el motivo que sea y en cualquier momento.</p>
<p>
Nos reservamos el derecho de obligar a que se produzca la confiscación de cualquier nombre de usuario por el motivo que sea.</p>
<p>
Nos reservamos el derecho de modificar o finalizar el servicio o el acceso a este por cualquier motivo, sin previo aviso, en cualquier momento y sin ninguna responsabilidad contigo. Puedes desactivar tu cuenta de sclusib. Para ello envíanos un mail a <a href="mailto:soporte@sclusib.com">soporte@sclusib.com</a> Si dejamos de proporcionarte acceso al servicio o utilizas el mail mencionado anteriormente para desactivar la cuenta, entonces las fotos, los comentarios, los indicadores de que algo te gusta, los contactos y todos los demás datos dejarán de estar accesibles a través de tu cuenta (por ejemplo, los usuarios no podrán acceder a tu nombre de usuario ni ver tus fotos), pero dichos materiales y datos pueden conservarse y aparecer en el servicio (por ejemplo, si otros usuarios compartieron a su vez tu Contenido).
</p>
<h2>
Actualización términos y condiciones
</h2>
<p>
Queda a nuestra entera discreción reservarnos el derecho de cambiar estas condiciones de uso periódicamente. A menos que realicemos un cambio por motivos legales o administrativos, avisaremos de la entrada en vigor de las condiciones actualizadas con una antelación razonable. Aceptas que podemos notificar dichas condiciones actualizadas mediante su publicación en el servicio y que el uso que hagas del servicio tras la fecha de entrada en vigor de las condiciones actualizadas (o la participación en otra conducta que podamos especificar de forma razonable) constituye tu aceptación de las condiciones actualizadas. Por lo tanto, debes revisar estas condiciones de uso y cualquier Condición actualizada antes de utilizar el servicio. Las condiciones actualizadas entrarán en vigor a partir del momento en el que se publiquen, o en una fecha posterior que pueda especificarse en las condiciones actualizadas, y se aplicarán al uso que realices del servicio a partir de ese momento. Los conflictos que surjan antes de la fecha de entrada en vigor de las condiciones actualizadas estarán sujetos a estas condiciones de uso.
</p>
<h2>
Publicidad
</h2>
<p>
Parte del servicio se financia mediante ingresos publicitarios y puede mostrar anuncios y promociones y, por la presente, aceptas que sclusib puede insertar estos anuncios y promociones en el servicio o sobre tu contenido, acerca de este o junto con este. La manera, el modo y la extensión de estos anuncios y estas promociones están sujetos a cambios sin que sea necesario avisarlo de forma específica.</p>
<h2>
Descargo de responsabilidad de las garantías
</h2>
<p>
El servicio de sclusib se proporciona “tal cual”, “como esté disponible” y “con todos los fallos” sin garantía alguna expresa o implícita. No garantizamos que funcione siempre sin interrupciones, retrasos o imperfecciones.</p>
<p>
Aceptas que el uso del servicio corre bajo tu responsabilidad por lo que descargamos específicamente dicha responsabilidad.
</p>