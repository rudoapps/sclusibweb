<h1>Preguntas frecuentes</h1>
<h2>¿Es una aplicación totalmente separada de Twitter e Instagram</h2>
<p>Si. Sclusib es una aplicación nueva en la que tus seguidores también tendrán que seguirte. Cuantos más seguidores consigas, más personas verán tus fotos y más dinero para ti.</p>
 <h2>
¿Es una app gratuita?</h2>
<p>Si. Totalmente gratuita tanto para los influencers como para los seguidores. Estos verán publicidad y esa es la forma en la que obtenemos ingresos y podemos pagarte.</p>
<h2>
¿Los usuarios no pueden subir fotos?</h2>
<p>No. En sclusib sólo pueden subir fotos los influencers verificados con los que tengamos un acuerdo. Los fans podrán seguirte, comentar, compartir o decir que les gustan tus fotos pero no podrán subir contenido. </p>
 <h2>
¿Me pagáis por publicitar sclusib en mis redes sociales? </h2>
<p>No. Sclusib es una red social que te paga por subir fotos en nuestra app en función del número de personas que las vean. Necesitamos que te hagas un perfil y lo mantengas activo. No te pagamos por subir fotos a tu Instagram o a tu Twitter. Cuando más publicites tus fotos en sclusib más gente las verá y más dinero ganarás. </p>
 
<h2>Si quiero ganar dinero con sclusib ¿tengo que dejar de usar otras redes sociales?</h2>
<p>No. Sclusib es compatible con cualquier acuerdo que tengas (youtube, partners, MCM, managers, relación con marcas). Sigue usando tus otras redes de la forma habitual pero en sclusib pon fotos nuevas que no hayas puesto en otro sitio. Así tus fans tendrán un aliciente para ver tus fotos en sclusib.</p>

<h2>Si entro en sclusib y más adelante quiero salirme ¿puedo?</h2>
<p>Si. Si sclusib no te convence simplemente avísanos y borramos tu perfil. ¡Puedes irte cuando quieras!</p>
 
<h2>¿Cuánto dinero puedo ganar?</h2>
<p>Los ingresos generados dependen principalmente de dos factores.</p>
<ul>
<li>Del número de visualizaciones de tus fotos: cuanto más gente vea tus fotos más visualizaciones de publicidad y por tanto más dinero. El número de visualizaciones de fotos depende sobre todo del número de seguidores que tienes en sclusib y del número de fotos que subas.</li>
<li>Del tipo de anuncios que aparecen y del precio al que se venden estos anuncios: esto no lo puedes controlar tu, depende de Google que es nuestro proveedor publicitario.</li>
</ul>
<h2>¿Cómo se cuánto dinero estoy ganando?</h2>
<p>Dentro de tu perfil de sclusib podrás ver lo que has ganado ayer y lo que llevas ganado hasta la fecha. </p>
<h2>
¿Cómo consigo seguidores en sclusib?</h2>
<p>Animando a tus seguidores de otras redes a seguirte también en sclusib. Te daremos videos y fotos para que puedas compartir con frecuencia en tu Twitter y tu Instagram. De esta forma tus seguidores sabrán que hay una nueva forma de seguirte en la que van a poder estar más cerca de ti y ganar premios cuanto más la usen.</p>
 
<h2>¿Qué tipo de anuncios aparecerán?</h2>
<p>Nuestro plataforma publicitaria para anuncios es Google AdMob. Ellos decidirán el tipo y el formato de anuncios que colocarán. Sclusib no estará obligado a siempre poner anuncios junto a tus contenidos, dependerá de la disponibilidad en Google AdMob.</p>
 
<h2>¿Cómo recibiré los pagos?</h2>
<p>Sclusib te pagará a los 60 días aproximadamente tras la finalización de cualquier mes natural (enero, febrero…), siempre y cuando el balance a tu favor sea de al menos 50 euros en el momento en que el pago sea debido. Si no llegas a los 50 euros en ese mes por supuesto no los pierdes, acumulas el saldo mes tras mes hasta que llegues a los 50 euros.</p>
 <p>
No tendrás derecho a ganar o recibir ningún ingreso en relación con tu contenido si has utilizado imágenes sobre los que no tienes los derechos o si tienes actividad de clics no válidos.</p>
 <h2>
¿Qué son los clics no válidos?</h2>
<p>
La actividad de clics no válidos tiene lugar cuando se producen clics e impresiones que pueden aumentar de manera artificial los costes de un anunciante o los ingresos de un famoso. En estos casos, tomamos la decisión de no cobrar al anunciante. Esta actividad incluye, entre otros, los siguientes casos: clics o impresiones generados por alguien que hace clic en sus propios anuncios, un famoso que anima a hacer clic en sus anuncios, herramientas de clics y fuentes de tráfico automatizadas, robots y cualquier software destinado al uso fraudulento.</p>
 <p>
Tenga en cuenta que los clics en los anuncios deben proceder de usuarios interesados, por lo que cualquier método que genere clics o impresiones de forma artificial queda prohibido terminantemente según las políticas del programa.</p>
