{head}
<body>
	<body>
<div canvas="container">
<div class="header_static">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front">	
	<div class="container">								
				<div class="space"></div>	
	</div>
</div>
<div class="main_section color_front relative">
	<div class="container p40 all-influencers-line">													
		<div class="row">
			<div class="col-md-5 influencerImage">
			    <img src="{url_img_cover}">
			</div>
			<div class="col-md-7">
			    <h1 class="influencerName">{username}</h1>
			    <div class="pop-text">
			    <p>
			    	{text}
			    </p>			    	
			    </div>
			    <div class="space"></div>
			    <div class="row">
			    	<div class="col-md-6 col-xs-12 influencerSocial">
						<a href="https://www.instagram.com/{instagram}/"><img src="{url}img/social/ic_instagram.png" width="30px"></a>						
						<a href="https://twitter.com/{twitter}"><img src="{url}img/social/ic_twitter.png" width="30px"></a>
			    	</div>
			    	<div class="col-md-6 col-xs-12 influencerDownload">
						<a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="100"></a>		        		
			        	<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="100"></a>
			    	</div>
			    </div>
			</div>
		</div>
    </div>   
</div>  
{footer}
		</div>
<div off-canvas="slidebar-1 left reveal">
		<nav>
	<div class="menu-header-menu-container"><ul id="menu-header-menu-1" class="menu"><li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-611"><a href="http://www.sclusib.com/category/influencers/">Influencers</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-308"><a href="http://www.sclusib.com/top/">Top 100</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-262"><a href="http://www.sclusib.com/bio/">Bios</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-645"><a href="http://www.sclusib.com/descarga-sclusib-trivials-preguntas-influencers/">Trivials</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-309"><a href="http://www.sclusib.com/unete/">¿Eres influencer?</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-607"><a href="http://www.sclusib.com/category/celebrities/">Celebrities</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-608"><a href="http://www.sclusib.com/category/estilo/">Estilo</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-609"><a href="http://www.sclusib.com/category/estilo/beauty/">Beauty</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-610"><a href="http://www.sclusib.com/category/estilo/fashion/">Fashion</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-612"><a href="http://www.sclusib.com/category/ocio/">Ocio</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-613"><a href="http://www.sclusib.com/category/ocio/cine/">Cine</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-615"><a href="http://www.sclusib.com/category/ocio/musica/">Música</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-614"><a href="http://www.sclusib.com/category/ocio/games/">Games</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-616"><a href="http://www.sclusib.com/category/tu-mundo/">Tu mundo</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-617"><a href="http://www.sclusib.com/category/tu-mundo/amistad/">Amistad</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-618"><a href="http://www.sclusib.com/category/tu-mundo/amor/">Amor</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-619"><a href="http://www.sclusib.com/category/tu-mundo/frases/">Frases</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-620"><a href="http://www.sclusib.com/category/tu-mundo/sexo/">Sexo</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-621"><a href="http://www.sclusib.com/category/tu-mundo/tus-preguntas/">Tus preguntas</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-696"><a href="http://www.sclusib.com/horoscopo/">Horóscopo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-695"><a href="http://www.sclusib.com/test/">Test</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-701"><a href="http://www.sclusib.com/sclusib/">¿sclusib?</a></li>
</ul></div>	
		</nav>
	</div> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>	
	<script src="http://www.sclusib.com/page/sidebar/slidebars.js"></script>
	<script src="http://www.sclusib.com/page/sidebar/scripts.js"></script>	
</body>
</html>