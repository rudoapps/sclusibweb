<?php

function mondo_shortcode_accordion( $atts, $content = null ) {

  $args = shortcode_atts( array(
    'openable' => false,
  ), $atts, 'accordion' );

  $openable = filter_var( $args['openable'], FILTER_VALIDATE_BOOLEAN );

  $accordion_class = 'mondo-accordion';

  $openable ? $accordion_class .= ' openable' : '';

	return '<div class="' . $accordion_class . '">' . do_shortcode( $content ) . '</div>';
}
add_shortcode( 'accordion', 'mondo_shortcode_accordion' );

function mondo_shortcode_accordion_item( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'title'  => null,
		'opened' => false,
  ), $atts, 'accordion_item' );

	$title  = esc_attr( $args['title'] );
	$opened = filter_var( $args['opened'], FILTER_VALIDATE_BOOLEAN );

	$item_class = 'accordion-item';

	$opened ? $item_class .= ' open' : '';

  ob_start(); ?>

  <section class="<?php echo esc_attr( $item_class ); ?>">
  	<h2 class="entry-title waves-effect waves-block"><?php echo esc_html( $title ); ?></h2>
  	<div class="entry-content">
  		<?php echo do_shortcode( $content ); ?>
  	</div>
  </section>

  <?php

  return ob_get_clean();
}
add_shortcode( 'accordion_item', 'mondo_shortcode_accordion_item' );