<?php

function mondo_shortcode_icon( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'name' => null,
		'size' => 'small',
		'title' => null,
  ), $atts, 'icon' );

  $name = esc_attr( $args['name'] );
  $size = esc_attr( $args['size'] );
  $title = esc_attr( $args['title'] );

  return '<i class="mondo-icon mdi mdi-' . $name . ' ' . $size . '" title="' . $title . '"></i>';
}
add_shortcode( 'icon', 'mondo_shortcode_icon' );