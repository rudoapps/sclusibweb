<?php

function mondo_shortcode_youtube( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'id' => null,
  ), $atts, 'youtube' );

  $id = esc_attr( $args['id'] );

  $return = '<div class="mondo-video-wrapper">';
  $return .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $id . '?showinfo=0" allowfullscreen></iframe>';
  $return .= '</div>';

  return $return;
}
add_shortcode( 'youtube', 'mondo_shortcode_youtube' );

function mondo_shortcode_vimeo( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'id' => null,
  ), $atts, 'vimeo' );

  $id = esc_attr( $args['id'] );

  $return = '<div class="mondo-video-wrapper">';
  $return .= '<iframe src="https://player.vimeo.com/video/' . $id . '?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="960" height="540" allowfullscreen></iframe>';
  $return .= '</div>';

  return $return;
}
add_shortcode( 'vimeo', 'mondo_shortcode_vimeo' );