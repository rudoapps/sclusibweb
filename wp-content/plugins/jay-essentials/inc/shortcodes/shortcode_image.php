<?php

function mondo_shortcode_image( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'src'   => null,
		'alt'   => null,
		'link'  => null,
		'color' => null,
  ), $atts, 'image' );

	$src   = esc_url( $args['src'] );
	$alt   = esc_attr( $args['alt'] );
	$link  = esc_url( $args['link'] );
	$color = esc_attr( $args['color'] );

	$waves_class = 'mondo-image waves-effect';
	$color == 'light' ? $waves_class .= ' waves-light' : '';

	$return = '<span class="' . esc_attr( $waves_class ) . '">';
  $return .= '<img src="' . $src . '" alt="' . $alt . '">';
  $return .= '</span>';

  if ( ! is_null( $link ) ) {
  	$return = '<a href="' . $link . '">' . $return . '</a>';
  }

  return $return;
}
add_shortcode( 'image', 'mondo_shortcode_image' );