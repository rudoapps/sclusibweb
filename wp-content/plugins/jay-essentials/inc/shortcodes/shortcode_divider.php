<?php

function mondo_shortcode_divider( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'style' => 'mini',
  ), $atts, 'divider' );

  $style = esc_attr( $args['style'] );

  return '<div class="mondo-divider ' . $style . '"></div>';
}
add_shortcode( 'divider', 'mondo_shortcode_divider' );