<?php

function mondo_shortcode_grid( $atts, $content = null, $column ) {

	$args = shortcode_atts( array(
		'first'  => false,
		'last'   => false
  ), $atts, $column );

  $first = filter_var( $args['first'], FILTER_VALIDATE_BOOLEAN );
  $last = filter_var( $args['last'], FILTER_VALIDATE_BOOLEAN );

  $return = $column_class = '';
  $columns = array(
		'five_sixth'   => 'col-md-10',
		'three_fourth' => 'col-md-9',
		'two_third'    => 'col-md-8',
		'one_half'     => 'col-md-6',
		'one_third'    => 'col-md-4',
		'one_fourth'   => 'col-md-3',
		'one_sixth'    => 'col-md-2'
  );

  if ( array_key_exists( $column, $columns ) ) {
  	$column_class = $columns[ $column ];
  }

  // If it's the first column, then start a row.
  if ( $first ) {
  	$return .= '<div class="row">';
  }

  $return .= '<div class="' . $column_class . '">' . do_shortcode( $content ) . '</div>';

  // If it's the last column, then end a row.
  if ( $last ) {
  	$return .= '</div>';
  }

  return $return;
}

function mondo_shortcode_five_sixth( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'five_sixth' );
}
add_shortcode( 'five_sixth', 'mondo_shortcode_five_sixth' );

function mondo_shortcode_three_fourth( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'three_fourth' );
}
add_shortcode( 'three_fourth', 'mondo_shortcode_three_fourth' );

function mondo_shortcode_two_third( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'two_third' );
}
add_shortcode( 'two_third', 'mondo_shortcode_two_third' );

function mondo_shortcode_one_half( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'one_half' );
}
add_shortcode( 'one_half', 'mondo_shortcode_one_half' );

function mondo_shortcode_one_third( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'one_third' );
}
add_shortcode( 'one_third', 'mondo_shortcode_one_third' );

function mondo_shortcode_one_fourth( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'one_fourth' );
}
add_shortcode( 'one_fourth', 'mondo_shortcode_one_fourth' );

function mondo_shortcode_one_sixth( $atts, $content = null ) {

	return mondo_shortcode_grid( $atts, $content, 'one_sixth' );
}
add_shortcode( 'one_sixth', 'mondo_shortcode_one_sixth' );