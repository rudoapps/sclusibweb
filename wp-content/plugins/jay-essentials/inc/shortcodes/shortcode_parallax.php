<?php

function mondo_shortcode_parallax( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'bg'     => null,
		'height' => null,
  ), $atts, 'parallax' );

	$bg     = $args['bg'];
	$height = esc_attr( $args['height'] );

  $css = '';
  is_null( $bg ) ? '' : $css .= 'background-image: url(' . $bg . ');';
  is_null( $height ) ? '' : $css .= 'height: ' . $height . 'px;';

  return '<div class="mondo-parallax" data-stellar-background-ratio="0.2" style="' . $css . '"></div>';
}
add_shortcode( 'parallax', 'mondo_shortcode_parallax' );