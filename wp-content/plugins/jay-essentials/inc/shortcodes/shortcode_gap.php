<?php

function mondo_shortcode_gap( $atts, $content = null ) {

	$args = shortcode_atts( array(
		'height' => '20',
  ), $atts, 'gap' );

  $height = esc_attr( $args['height'] );

  $css = 'height:' . $height . 'px;';

  return '<div class="mondo-gap" style="' . $css . '"></div>';
}
add_shortcode( 'gap', 'mondo_shortcode_gap' );