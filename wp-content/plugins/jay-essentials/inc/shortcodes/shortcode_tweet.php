<?php

function mondo_shortcode_tweet( $atts, $content = null ) {

	$url = esc_url( get_permalink() );

	return '<span class="mondo-tweetable" data-url="' . $url . '">' . $content . '</span>';
}
add_shortcode( 'tweet', 'mondo_shortcode_tweet' );