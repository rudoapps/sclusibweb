<?php
/*
Plugin Name: Jay Essentials
Description: A plugin which adds main functionalities to Jay.
Version: 1.2
Author: MondoTheme
Author URI: http://themeforest.net/user/mondotheme
*/

define( 'JAY_ESSENTIALS_VERSION', '1.2' );
define( 'JAY_ESSENTIALS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'JAY_ESSENTIALS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_grid.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_tweet.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_icon.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_video.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_divider.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_accordion.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_gap.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_image.php';
require JAY_ESSENTIALS_PLUGIN_DIR . '/inc/shortcodes/shortcode_parallax.php';

/*
 * This function removed unwanted <p> and <br> tags from shortcode output
 * It doesn't affect other plugin's shortcodes, but only affects this theme's custom shortcodes
 * The snippet is from bitfade and the gist is here https://gist.github.com/bitfade/4555047
 * You can read the discussion about this snippet from here http://themeforest.net/forums/thread/how-to-add-shortcodes-in-wp-themes-without-being-rejected/98804?page=4#996848
 */
function mondo_the_content_filter( $content ) {

	// array of custom shortcodes requiring the fix 
	$block = join( '|', array( 'five_sixth', 'three_fourth', 'two_third', 'one_half', 'one_third', 'one_fourth', 'one_sixth', 'youtube', 'vimeo', 'divider', 'accordion', 'accordion_item', 'gap', 'image', 'parallax' ) );

	// opening tag
	$rep = preg_replace( "/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]", $content );
		
	// closing tag
	$rep = preg_replace( "/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]", $rep );

	return $rep;
}
add_filter( 'the_content', 'mondo_the_content_filter' );