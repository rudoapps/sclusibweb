<?php
	global $mondo_admin_data;

	$main_layout = $mondo_admin_data['main_layout'];
	$content_column_class = 'col-md-8';
	$sidebar_column_class = 'col-md-4';
	$layout = rwmb_meta( 'mondo_single_layout' );

	if ( $main_layout == 'left_sidebar' ) {
		$content_column_class .= ' col-md-push-4';
		$sidebar_column_class .= ' col-md-pull-8 left-column';
	} else if ( $main_layout == 'full_width' ) {
		$content_column_class = 'col-md-12';
	}

	if ( $layout == 'full_width' ) {
		$content_column_class = 'col-md-12';	
	}

	$author_disabled = true;
?>

<?php get_header(); ?>
	
	<div class="<?php echo esc_attr( $content_column_class ); ?>">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'inc/post-formats/standard/content-single', get_post_format() );

					if ( $mondo_admin_data['enable_author'] == '1' ) {
						mondo_about_author();
						$author_disabled = false;
					}
					if ( $mondo_admin_data['enable_relate'] == '1' ) {
						mondo_related_posts( $author_disabled );
					}
					//the_post_navigation();

					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				?>

			<?php endwhile; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>

	<?php if ( $main_layout != 'full_width' && $layout != 'full_width' ) : ?>
		<div class="<?php echo esc_attr( $sidebar_column_class ); ?>">
			<?php get_sidebar(); ?>
		</div>
	<?php endif; ?>
	
<?php get_footer(); ?>
