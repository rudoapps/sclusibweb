<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo apply_filters( 'mondo_search_field_placeholder', __( 'Enter keyword...', 'mondo' ) ); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'mondo' ); ?>">
	<input type="submit" class="search-submit waves-effect waves-button waves-float button-primary" value="<?php echo apply_filters( 'mondo_search_submit_text', esc_attr_x( 'Search', 'submit button', 'mondo' ) ); ?>">
</form>