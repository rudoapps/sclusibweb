<?php
	global $mondo_admin_data;

	$content_column_class = 'col-md-8';
	$sidebar_column_class = 'col-md-4';
	$main_layout = $mondo_admin_data['main_layout'];

	if ( $main_layout == 'full_width' ) {
		$content_column_class = 'col-md-12';
	} else if ( $main_layout == 'left_sidebar' ) {
		$content_column_class .= ' col-md-push-4';
		$sidebar_column_class .= ' col-md-pull-8 left-column';
	}
?>

<?php get_header(); ?>
	
	<div class="<?php echo esc_attr( $content_column_class ); ?>">
		<section id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php
					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'content', 'search' );
					?>

				<?php endwhile; ?>

				<?php
					if ( $mondo_admin_data['posts_navigation'] == 'link' ) {
						the_posts_navigation();
					} else {
						mondo_pagination();
					}
				?>

			<?php else : ?>

				<?php get_template_part( 'content', 'none' ); ?>

			<?php endif; ?>

			</main><!-- #main -->
		</section><!-- #primary -->
	</div>

	<?php if ( $main_layout != 'full_width' ) : ?>

		<div class="<?php echo esc_attr( $sidebar_column_class ); ?>">
			<?php get_sidebar(); ?>
		</div>

	<?php endif; ?>
	
<?php get_footer(); ?>
