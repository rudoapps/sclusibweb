<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Jay
 */

if ( ! is_active_sidebar( 'widget-sidebar' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<?php
		if ( class_exists( 'WooCommerce' ) && ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) ) {
			dynamic_sidebar( 'widget-shop' );
		} else {
			dynamic_sidebar( 'widget-sidebar' );
		}
	?>
</div><!-- #secondary -->
