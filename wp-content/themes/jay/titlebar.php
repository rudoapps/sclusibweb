<?php
	global $mondo_admin_data;
	$titlebar = rwmb_meta( 'mondo_titlebar_style' );
	$custom_title = rwmb_meta( 'mondo_titlebar_title' );
	$subtitle_output = rwmb_meta( 'mondo_titlebar_subtitle' );
	$title_output = '';
	$skrollr_full = $skrollr_wide = false;
	
	$titlebar == '' ? $titlebar_class = 'thin' : $titlebar_class = $titlebar;

	if ( ! wp_is_mobile() ) {
		if (
			( is_home() && $mondo_admin_data['blog_titlebar_style'] == 'full' ) ||
			( class_exists( 'WooCommerce' ) && is_shop() && $mondo_admin_data['shop_titlebar_style'] == 'full' ) ||
			( is_singular() && $titlebar == 'full' )
		) {
			$skrollr_full = true;
		} elseif (
			( is_home() && $mondo_admin_data['blog_titlebar_style'] == 'wide' ) ||
			( class_exists( 'WooCommerce' ) && is_shop() && $mondo_admin_data['shop_titlebar_style'] == 'wide' ) ||
			( is_singular() && $titlebar == 'wide' )
		) {
			$skrollr_wide = true;
		}
	}

	if ( is_home() ) {

		$titlebar_class = $mondo_admin_data['blog_titlebar_style'];

		if ( isset( $mondo_admin_data['blog_titlebar_title'] ) && mondo_redux_text_set( $mondo_admin_data['blog_titlebar_title'] ) ) {
			$title_output = $mondo_admin_data['blog_titlebar_title'];
		} else {
			$title_output = get_bloginfo( 'name' );
		}

		if ( isset( $mondo_admin_data['blog_titlebar_subtitle'] ) && mondo_redux_text_set( $mondo_admin_data['blog_titlebar_subtitle'] ) ) {
			$subtitle_output = $mondo_admin_data['blog_titlebar_subtitle'];
		} else {
			$subtitle_output = get_bloginfo( 'description' );
		}		

	} else if ( is_singular( 'post' ) && $titlebar != 'thin' ) {

		$custom_title == '' ? $title_output = '' : $title_output = $custom_title;

	} else if ( class_exists( 'WooCommerce' ) && is_shop() ) {

		$titlebar_class = $mondo_admin_data['shop_titlebar_style'];

		if ( isset( $mondo_admin_data['shop_titlebar_title'] ) && mondo_redux_text_set( $mondo_admin_data['shop_titlebar_title'] ) ) {
			$title_output = $mondo_admin_data['shop_titlebar_title'];
		} else {
			$title_output = __( 'Jay Shop', 'mondo' );
		}

		if ( isset( $mondo_admin_data['shop_titlebar_subtitle'] ) && mondo_redux_text_set( $mondo_admin_data['shop_titlebar_subtitle'] ) ) {
			$subtitle_output = $mondo_admin_data['shop_titlebar_subtitle'];
		} else {
			$subtitle_output = get_bloginfo( 'description' );
		}		

	} else if ( is_archive() ) {

		if ( is_category() ) {
			$titlebar_class = 'thin';
			$title_output = __( 'Browsing Category', 'mondo' );
			$subtitle_output = get_the_archive_title();
		} else if ( is_tag() ) {
			$titlebar_class = 'thin';
			$title_output = __( 'Browsing Tag', 'mondo' );
			$subtitle_output = get_the_archive_title();
		} else if ( is_date() ) {
			$titlebar_class = 'thin';
			$title_output = __( 'Browsing Date', 'mondo' );
			$subtitle_output = get_the_archive_title();
		} else {
			$title_output = get_the_archive_title();
		}

	} else if ( is_search() ) {

		$titlebar_class = 'thin';
		$title_output = __( 'Search Results for:', 'mondo' );
		$subtitle_output = get_search_query();

	} else {

		$title_output = get_the_title();
		$custom_title == '' ? '' : $title_output = $custom_title;

	}
?>

<?php if (
	( is_home() && $mondo_admin_data['blog_titlebar_style'] != 'no' ) ||
	( class_exists( 'WooCommerce' ) && is_shop() && $mondo_admin_data['shop_titlebar_style'] != 'no' ) ||
	is_category() ||
	is_tag() ||
	is_date() ||
	is_search() ||
	( is_page() && $titlebar != 'no' ) ||
	( is_singular( 'post' ) && $titlebar != 'no' && $titlebar != '' )
) : ?>
	<section id="mondo-titlebar" class="<?php echo esc_attr( $titlebar_class ); ?>">
		<?php if (
			( is_home() && isset( $mondo_admin_data['blog_titlebar_blur'] ) && mondo_redux_image_set( $mondo_admin_data['blog_titlebar_blur'] ) && $mondo_admin_data['blog_titlebar_style'] != 'thin' ) ||
			( class_exists( 'WooCommerce' ) && is_shop() && isset( $mondo_admin_data['shop_titlebar_blur'] ) && mondo_redux_image_set( $mondo_admin_data['shop_titlebar_blur'] ) && $mondo_admin_data['shop_titlebar_style'] != 'thin' ) ||
			( is_singular() && rwmb_meta( 'mondo_titlebar_blur' ) != '' && rwmb_meta( 'mondo_titlebar_style' ) != 'thin' )
		) : ?>
			<div class="blur" data-0="opacity: 0;" data-400="opacity: 1;"></div>
		<?php endif; ?>

		<?php if ( $skrollr_full ) : ?>
		<div class="container" data-0="transform: translateY(-50%); opacity: 1;" data-600="transform: translateY(100%); opacity: 0;">
		<?php elseif ( $skrollr_wide ) : ?>
		<div class="container" data-0="transform: translateY(-50%); opacity: 1;" data-460="transform: translateY(50%); opacity: 0;">
		<?php else : ?>
		<div class="container">
		<?php endif ?>
			<h1 class="page-title"><?php echo esc_html( $title_output ); ?></h1>

			<?php if ( $subtitle_output != '' ) : ?>
				<h2 class="page-subtitle"><?php echo esc_html( $subtitle_output ); ?></h2>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>