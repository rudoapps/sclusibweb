	<?php get_header(); ?>
	<div class="row">
		<div class="col-md-8 ">
			<div class="row">
				<?php 					
					if ( have_posts() ) : while ( have_posts() ) : the_post();			  			
						get_template_part( 'page-single', get_post_format() );																			
					endwhile; endif; 
				?>					
			</div>	
			<div class="space40"></div>			
		</div>		
	<!--SIDEBAR-->
	<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>