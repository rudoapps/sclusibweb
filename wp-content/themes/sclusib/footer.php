</div>

<div class="space40">

<div class="footer">
	<div class="container">
		<div class="row">

			<div class="col-md-4">
				<img src="<?php bloginfo('template_directory');?>/img/sclusib_logo_blanco.png" class="footer-logo">
				 
				<p>© Sclusib Studio S.L.<br/>
				C/Sorni 7 - 46004 - Valencia. <a href="mailto:hola@sclusib.com">hola@sclusib.com</a></p>
				
				<p>
				<a href="http://www.sclusib.com/quienes-somos/">Quiénes somos</a><br/>
				<a href="http://www.sclusib.com/contacto/">Contacta con nosotros</a><br/>
				<a href="http://www.sclusib.com/anunciate/">Anúnciate</a></p>
			</div>
			<div class="col-md-8">
				<div class="footer-social">
					<a href="https://twitter.com/sclusib" target="blank"><img src="<?php bloginfo('template_directory');?>/img/twitter_round.png"></a>
        			<a href="https://www.facebook.com/sclusib/" target="blank"><img src="<?php bloginfo('template_directory');?>/img/facebook_round.png"></a>
        			<a href="https://www.instagram.com/sclusib/" target="blank"><img src="<?php bloginfo('template_directory');?>/img/instagram_round.png"></a>
        			<a href="https://plus.google.com/118041313063456814322" target="blank"><img src="<?php bloginfo('template_directory');?>/img/google-plus.png"></a>
        			<!--<img src="<?php bloginfo('template_directory');?>/img/rss_round.png">-->
				</div>
				
				<div class="footer-subscribe">
					<h2>Suscríbete!</h2>
					<p>¿Quieres estar al día de todas nuestras noticias? Suscríbete a nuestra newsletter.<br/>
					No nos gusta el spam, podrás darte de baja cuando quieras.</p>				
					<form id="subscribe-form">
					<input type="email" name="email" class="subscribe-email" placeholder="Escribe tu email"><button id="subscribe">Aceptar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="footer-secondary">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-6">
				<ul >
					<li><a href="http://www.sclusib.com/category/influencers/"><b>Influencers</b></a></li>
					<li><a href="http://www.sclusib.com/top/">Top 100</a></li>
					<li><a href="http://www.sclusib.com/bio/">Bios</a></li>
					<li><a href="http://www.sclusib.com/descarga-sclusib-trivials-preguntas-influencers/">Trivials</a></li>
					<li><a href="http://www.sclusib.com/unete/">¿Eres influencer?</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="http://www.sclusib.com/category/celebrities/"><b>Celebrities</b></a></li>
				</ul>
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="http://www.sclusib.com/category/estilo/"><b>Estilo</b></a>
					<li><a href="http://www.sclusib.com/category/estilo/beauty/">Beauty</a></li>
					<li><a href="http://www.sclusib.com/category/estilo/fashion/">Fashion</a></li>
				<ul>	
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="http://www.sclusib.com/category/ocio/"><b>Ocio</b></a></li>
					<li><a href="http://www.sclusib.com/category/ocio/cine/">Cine</a></li>
					<li><a href="http://www.sclusib.com/category/ocio/musica/">Música</a></li>
					<li><a href="http://www.sclusib.com/category/ocio/games/">Games</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="http://www.sclusib.com/category/tu-mundo/"><b>Tu mundo</b></a></li>
					<li><a href="http://www.sclusib.com/category/tu-mundo/amistad/">Amistad</a></li>
					<li><a href="http://www.sclusib.com/category/tu-mundo/amor/">Amor</a></li>
					<li><a href="http://www.sclusib.com/category/tu-mundo/frases/">Frases</a></li>
					<li><a href="http://www.sclusib.com/category/tu-mundo/sexo/">Sexo</a></li>
					<li><a href="http://www.sclusib.com/category/tu-mundo/tus-preguntas/">Tus preguntas</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-xs-6">
				<ul>
					<li><a href="http://www.sclusib.com/horoscopo/"><b>Horóscopo</b></a></li>
					<li><a href="http://www.sclusib.com/test/"><b>Test</b></a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>



</div>

</div>
<div off-canvas="slidebar-1 left reveal">
	<nav>
	<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
	</nav>
</div> 
<script src="<?php bloginfo('template_directory');?>/sidebar/slidebars.js"></script>
<script src="<?php bloginfo('template_directory');?>/sidebar/scripts.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61907046-1', 'auto');
  ga('send', 'pageview');
</script>
</body>
</html>