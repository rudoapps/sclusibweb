				<div class="col-md-6">
					<div class="post">
						<div class="post-related-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
							<?php
							$category = get_the_category(); 
							?>
							<div class="post-related-tag" style="background-color:<?php sclusib_colors($category[0]->cat_ID);?>;"><?php the_category(); ?></div>
						</div>
						<div class="post-related-content">
							<h2><a href="<?php  the_permalink();?>"><?php the_title(); ?></a></h2>							
						</div>
						<div class="clear"></div>

					</div>	
				</div>