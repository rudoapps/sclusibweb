	<?php get_header(); ?>
	<div class="row">
		<div class="col-md-8 ">
			<div class="row">
				<?php
					//query_posts('limit=1');
					if ( have_posts() ) : while ( have_posts() ) : the_post();
			  			get_template_part( 'content-single', get_post_format() );
			  			$id= get_the_ID();			  			
					endwhile; endif; 
				?>		
				<div class="space-text">
				<h3>Quizás te interese</h3>	
				</div>
				<?php
					$related = new WP_Query( ( array( 'category__in' => wp_get_post_categories($id), 'posts_per_page' => 4, 'post__not_in' => array($id) ) ));
					if( $related->have_posts() ) { 
					  while( $related->have_posts() ) { $related->the_post(); 
						get_template_part( 'content-related', get_post_format() );
					  }
					}
					wp_reset_postdata();
					?>
			</div>	

			<div class="space40"></div>		
			
		</div>	
<!--SIDEBAR-->
	<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>