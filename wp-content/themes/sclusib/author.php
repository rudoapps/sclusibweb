	<?php get_header(); ?>
	<div class="row">
		<div class="col-md-8 ">
					<div class="post">
						
							<div class="post-author-image">
								<?php
									global $wp_query;
									$curauth = $wp_query->get_queried_object();
								?>

								<?php echo get_avatar( $curauth->ID);?> 
							</div>
						
							<div class="post-author-info">
								 <h1><?php echo $curauth->nickname; ?></h1>

								<p><?php echo $curauth->description; ?></p>
							</div>
							<div class="clear"></div>
					</div>
				
			<div class="row">
				<?php 
					//query_posts('limit=1');
					$count = 0;
					if ( have_posts() ) : while ( have_posts() ) : the_post();
			  			if($count==0){
							get_template_part( 'first', get_post_format() );
						}else{
							get_template_part( 'content', get_post_format() );
						}
						$count ++;
					endwhile; endif; 
				?>					
			</div>	
			<div class="row">
			<div class="navigation">
				<div class="alignleft"><?php previous_posts_link( '&laquo; Más recientes' ); ?></div>
				<div class="alignright"><?php next_posts_link( 'Más antiguas &raquo;', '' ); ?></div>
			</div>
			</div>
			<div class="space40"></div>			
		</div>		
	<!--SIDEBAR-->
	<?php get_sidebar(); ?>
	</div>

		
<?php get_footer(); ?>

<?php include('wp-content/themes/sclusib/newsletter.php'); ?>
<?php include('wp-content/themes/sclusib/popup.php'); ?>