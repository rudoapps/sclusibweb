				<div class="col-md-6">
					<div class="post">
						<div class="post-top" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
							<?php
							$category = get_the_category(); 
							?>
							<div class="post-tag" style="background-color:<?php sclusib_colors($category[0]->cat_ID);?>;"><?php the_category(); ?></div>
						</div>
						<div class="post-bottom">
							<h1><a href="<?php  the_permalink();?>"><?php the_title(); ?></a></h1>
							<div class="post-bar">
								<div class="post-bar-author">
									<?php echo get_avatar( get_the_author_meta('ID'));?>  <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
								</div>
								<div class="post-bar-items">
									<!--<img src="<?php bloginfo('template_directory');?>/img/comments.jpg">
									<img src="<?php bloginfo('template_directory');?>/img/likes.jpg">-->
								</div>								
							</div>
							<?php the_content(); ?>
							<div class="post-bar-footer">
								<a href="https://www.twitter.com/intent/tweet?text=<?php the_title(); ?>&original_referer=<?php the_title(); ?>&url=<?php  the_permalink();?>"><img src="<?php bloginfo('template_directory');?>/img/twitter_post.png"></a>
								<a href="https://www.facebook.com/sharer.php?u=<?php  the_permalink();?>"><img src="<?php bloginfo('template_directory');?>/img/facebook_post.png"></a>
							</div>
							<div class="post-bar-footer-time">
									<?php the_date(); ?>
								</div>
						</div>
						<div class="clear"></div>

					</div>	
				</div>