				<div class="col-md-12">
					<div class="post">						
						<div class="post-bottom-complete">
							<h1><?php the_title(); ?></h1>
							<div class="post-bar">
								<div class="post-bar-author">
									<?php the_author(); ?> | <?php the_date(); ?>	
								</div>							
							</div>
							<div class="post-content-complete">
								<?php the_content(); ?>
							</div>
						
						</div>
						<div class="clear"></div>

					</div>	
				</div>