<?php
function register_my_menu() {
  register_nav_menu('new-menu',__( 'New Menu' ));
}
add_action( 'init', 'register_my_menu' );
add_theme_support( 'post-thumbnails' );

add_filter('wp_nav_menu_items','add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {
    if( $args->theme_location == 'primary' )
        return $items.get_search_form();

    return $items;
}

function themename_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'theme_name' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );

}
add_action( 'widgets_init', 'themename_widgets_init' );

function sclusib_colors($id){
	$category[101] = "#3FB8AF";//INFLUENCERS
	$category[102] = "#7FC7AF";//CELEBRITIES
	$category[146] = "#DAD8A7";//MUSICA
	$category[147] = "#DAD8A7";//CINE
	$category[148] = "#DAD8A7";//GAMES
	$category[149] = "#FF9E9D";//ESTILO
	$category[151] = "#FF9E9D";//FASHION
	$category[150] = "#FF9E9D";//BEAUTY
	$category[152] = "#FF3D7F";//TU MUNDO
	$category[154] = "#FF3D7F";//AMISTAD08
	$category[153] = "#FF3D7F";//AMOR
	$category[155] = "#FF3D7F";//SEXO
	$category[156] = "#FF3D7F";//TUS PREGUNTAS
	$category[157] = "#FF3D7F";//FRASES
	$category[212] = "#D302BB";//HOROSCOPO
	$category[213] = "#3FB8AF";//TEST
	$category[101] = "#0EF4B3";//INFLUENCERS
	
	if($category[$id]==""){
		echo "#fa5669";
	}else{
		echo $category[$id];
	}
	
}

function sclusib_get_color($id) {
	$category["Influencers"] = "#3FB8AF";
	$category["Celebrities"] = "#7FC7AF";
	$category["Ocio"] 		 = "#DAD8A7";
	$category["Estilo"] 	 = "#FF9E9D";
	$category["Tu mundo"] 	 = "#FF3D7F";
	$category["Horóscopo"] 	 = "#D302BB";
	$category["Test"] 		 = "#3FB8AF";
	$category["Influencers"] = "#0EF4B3";
	
	return $category[$category_name];
}


 
/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/
 
function et_excerpt_more($more) {
    global $post;
    return '<div class="view-full-post"><a href="'. get_permalink($post->ID) . '" class="view-full-post-btn">View Full Post</a></div>;';
}
add_filter('excerpt_more', 'et_excerpt_more');

?>