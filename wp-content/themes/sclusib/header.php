<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Sclusib. La revista para adolescentes que quieren comerse el mundo.</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Noticias diarias sobre los famosos y los influencers que lo petan en youtube, instagram y Twitter. Además música, cine, moda, belleza y los mejores consejos sobre amor y amistad. " />
<?php wp_head();?>
<!-- Bootstrap -->
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/bootstrap/css/bootstrap-theme.min.css">
<script src="<?php bloginfo('template_directory');?>/bootstrap/js/bootstrap.min.js"></script>
<!-- Slider -->
<script src="<?php bloginfo('template_directory');?>/jquery/jquery-3.1.1.min.js"></script>
<script src="<?php bloginfo('template_directory');?>/jquery/jquery.cookie.js"></script>
<script src="<?php bloginfo('template_directory');?>/slider/js/unslider-min.js"></script> <!-- but with the right path! -->
<!-- Sidebar -->
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/sidebar/slidebars.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/sidebar/style.css">
<!-- CSS -->
<!--<link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,300' rel='stylesheet' type='text/css'>-->
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/style.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/general.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/small.css">
<script>
$( document ).ready(function() {
var close = true
$("#button-comments").click(function(){
    $("#block-comments").toggle();
    if(close){
        close = false;
        $("#button-comments").text('Cerrar comentarios');
    }else{
        close = true;  
        $("#button-comments").text('Ver comentarios');
    } 
});

    <?php include('wp-content/themes/sclusib/popup-header.php'); ?>
    //alert("ad: " + Cookies.get('anuncio') + "/" + ad_show + " | newsletter: " + Cookies.get('newsletter') + "/" +newsletter_show );
    if(Cookies.get('anuncio')== undefined && ad_show){
        $('[data-popup="popup-ad"]').delay(3000).fadeIn();
        Cookies.set('anuncio', 'visto', {expires: ad_expires});
    }else{
        if(Cookies.get('newsletter')== undefined && newsletter_show){
            $('[data-popup="popup-newsletter"]').delay(3000).fadeIn();
            Cookies.set('newsletter', 'visto', {expires: newsletter_expires});
        }
    }
    /*
    //Descomentar si finalmente se hace un sticky menu
    $('.main_window').scroll(function() {
      if ($(this).scrollTop() > 1){  
          $('.topheader').addClass("sticky");
          $('.topheader_transparent').addClass("sticky");
      }
      else{
          $('.topheader').removeClass("sticky");
          $('.topheader_transparent').removeClass("sticky");
      }
  });
  */

}); 

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e)  {
        var targeted_popup_class = jQuery(this).attr('data-popup-open');
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

        e.preventDefault();
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e)  {

        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        //alert("cerrar: " + targeted_popup_class);
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});



$( window ).resize(function() {

  	if($( window ).width()>768){
	  	$("#menu-toggle-btn").hide();	
		$( ".nav_menu" ).show();
	}else{		
		$("#menu-toggle-btn").show();
		$( ".nav_menu" ).hide();		
	}
});


(function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:207035,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');

</script>
</head>
<body>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "sclusib",
  "url" : "https://www.sclusib.com",
  "sameAs" : [
    "https://twitter.com/sclusib",
    "https://plus.google.com/118041313063456814322",
    "https://www.facebook.com/sclusib",
    "https://www.instagram.com/sclusib"
 ]
}
</script> 
<div canvas="container" class="main_window">
<div class="header color_back">	
	<div class="container">
        <div class="row">
            <div class="topheader">
            <div class="col-md-2">
	            <div id="social-menu">
                    <a href="https://plus.google.com/118041313063456814322" target="blank"><img src="<?php bloginfo('template_directory');?>/img/google-plus.png"></a>
	                <a href="https://twitter.com/sclusib" target="blank"><img src="<?php bloginfo('template_directory');?>/img/twitter_round.png"></a>
        			<a href="https://www.facebook.com/sclusib/" target="blank"><img src="<?php bloginfo('template_directory');?>/img/facebook_round.png"></a>
        			<a href="https://www.instagram.com/sclusib/" target="blank"><img src="<?php bloginfo('template_directory');?>/img/instagram_round.png"></a>

                </div>
	            <span class="js-toggle-left-slidebar">
                <div id="menu-toggle-btn">
                    <span class="menu-line"></span>
                    <span class="menu-line"></span>
                    <span class="menu-line"></span>
                </div>
                </span>

                <a href="http://www.sclusib.com"><img src="/page/img/logo-sclusib.png" class="logo"></a>
                <div class="clear"></div>

                
            </div>
                       <div class="col-md-10">
	                            
		
                <div class="nav_menu">
	                <nav>
						
	                    <?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
	                    
	                </nav> 
                </div>    
                                   
            </div>                
        </div>
        </div>
	</div>
	<div class="clear"></div>
</div>
<div class="space40"></div>

<div class="container">