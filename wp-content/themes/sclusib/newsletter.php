<div class="popup" data-popup="popup-newsletter">
	<div class="popup-inner">
		<h2>Suscríbete!</h2>
		<div class="popup-content">
			<p>¿Quieres estar al día de todas nuestras noticias? Suscríbete a nuestra newsletter.</p>
			<p>No nos gusta el spam, podrás darte de baja cuando quieras.</p>				
			<form id="subscribe-form">
			<input type="email" name="email" class="subscribe-email" placeholder="Escribe tu email"><button id="subscribe">Aceptar</button>
			</form>
		</div>
		<a class="popup-close" data-popup-close="popup-newsletter" href="#">x</a>
	</div>
</div>
<script type="text/javascript"> 

$(function() {
	$('#subscribe').click(function(){        
	    var myemail = $.trim($("input[name='email']").val());
        $('.popup-content').html("<b>Cargando...</b>");	        
		    $.post('subscribe.php', {  email: myemail}, function(data){	            
		        $('.popup-content').html("<b>"+data+"</b>");
		        $('.popup').delay(3000).fadeOut(300);
		        Cookies.remove('newsletter');
		        Cookies.set('newsletter', 'suscrito');
		    }).fail(function() {         	            
		     	$('.popup').delay(3000).fadeOut(300);
	    	});	 	       
	    return false;
    });
});
</script>