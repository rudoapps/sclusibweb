				<div class="col-md-12">
					<div class="post">
						<div class="post-top-complete" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
							<?php
							$category = get_the_category(); 
							?>
							<div class="post-tag" style="background-color:<?php sclusib_colors($category[0]->cat_ID);?>;"><?php the_category(); ?></div>
						</div>
						<div class="post-bottom-complete">
							<h1><?php the_title(); ?></h1>
							<div class="post-bar">
								<div class="post-bar-author">
									<?php echo get_avatar( get_the_author_meta('ID'));?>  <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a> | <?php the_date(); ?>
									<!--
									<img src="<?php bloginfo('template_directory');?>/img/comments.jpg">
									<img src="<?php bloginfo('template_directory');?>/img/likes.jpg">-->
									<div class="post-bar-footer-complete">
									<a href="https://www.twitter.com/intent/tweet?text=<?php the_title(); ?>&original_referer=<?php the_title(); ?>&url=<?php  the_permalink();?>"><img src="<?php bloginfo('template_directory');?>/img/twitter_post.png"></a>
									<a href="https://www.facebook.com/sharer.php?u=<?php  the_permalink();?>"><img src="<?php bloginfo('template_directory');?>/img/facebook_post.png"></a>
									</div>	
								</div>
								
												
							</div>
							<div class="post-content-complete">
<?php 
//the_content(); 	
$show_after_p = 2;
$content = apply_filters('the_content', $post->post_content);
$contents = explode("</p>", $content);
  	
  	$p_count = 1;
  	$max = count($contents);
	foreach($contents as $content){
		    echo $content;
		    if($p_count == $show_after_p){
		    	//include('wp-content/themes/sclusib/ad-post.php');
		    }

		    //penultimo parrafo
		    if($p_count == $max-2 && $max>6){
		    	//include('wp-content/themes/sclusib/ad-post.php');
		    }

		    echo "</p>";
		    $p_count++;
	}
?>
								<div id="button-comments">
									Ver comentarios
								</div>
								<div id="block-comments">
									<?php comments_template(); ?> 
								</div>
							</div>
							<!--<div class="post-bar-footer">
								<img src="<?php bloginfo('template_directory');?>/img/like_post.png">
								<img src="<?php bloginfo('template_directory');?>/img/twitter_post.png">
								<img src="<?php bloginfo('template_directory');?>/img/facebook_post.png">
								<img src="<?php bloginfo('template_directory');?>/img/g_post.png">								
							</div>-->
						
						</div>
						<div class="clear"></div>

					</div>	
				</div>