<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta name="twitter:card" content="app">
<meta name="twitter:site" content="@sclusib">
<meta name="twitter:description" content="Con sclusib Trivials podrás demostrar todo lo que sabes de tus influencers favoritos.">
<meta name="twitter:app:country" content="ES">
<meta name="twitter:app:name:iphone" content="sclusib">
<meta name="twitter:app:id:iphone" content="1154161195">
<meta name="twitter:app:url:iphone" content="https://itunes.apple.com/es/app/sclusib-trivials/id1154161195">
<meta name="twitter:app:name:googleplay" content="sclusib test">
<meta name="twitter:app:id:googleplay" content="com.sclusib.testsclusib">
<!--<meta name="twitter:app:url:googleplay" content="https://play.google.com/store/apps/details?id=com.sclusib.trivialsclusib">-->
<meta name="twitter:app:url:googleplay" content="https://play.google.com/store/apps/details?id=com.sclusib.trivialsclusib&hl=es">
<title>sclusib</title>
<script type="text/javascript">

var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }

    };

if ( isMobile.Android() ) {
	setTimeout (document.location.href = "https://play.google.com/store/apps/details?id=com.sclusib.test", 2000);
}
else if(isMobile.iOS())
{
	setTimeout (document.location.href="https://itunes.apple.com/es/app/sclusib-tests-amor-amistad/id1182967985?mt=8", 2000);
	
}
else
{
	setTimeout (document.location.href="http://www.sclusib.com", 2000);
	
}
</script>
</head>

<body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61907046-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>