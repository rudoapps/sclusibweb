<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<title>sclusib</title>
<meta name="description" content="" />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<script type="text/javascript" src="js/codigo.js"></script>
<meta name="Author" content="Marcos Plazas" />
<meta name="Keywords" content="red social, " />
<meta name="Robots" content="index, follow" />
<meta name="Googlebot" content="index, follow" />
<meta name="Copyright" content="" />
<meta name="Revisit" content="" />
<meta name="Distribution" content="Global" />
<meta http-equiv='Pragma' content='no-cache' />
<meta property="fb:app_id" content="" />
<meta property="fb:admins" content="" />
<meta property="og:type" content="" />
<meta property="og:title" content="" />
<meta property="og:image" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="">
<meta property="og:locale" content="es_ES" />
</head>

<body>

<header>
	<img class="logo" src="img/dp_logo_sclusib.png">
</header>

<content>
	<h3>Estamos haciendo mejoras muy pronto disponible de nuevo</h3>

</content>

<footer>	
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61907046-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
