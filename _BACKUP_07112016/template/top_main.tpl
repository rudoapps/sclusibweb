{head}
<body>
<div class="header color_front">	
	<div class="container">
		{menu}
	</div>
</div>
<div class="main_section color_front">	
	<div class="container">								
		<div class="space"></div>	
	</div>
</div>
<div class="main_section color_front">
	<div class="container">	
		<div class="space"></div>											
		<div class="row">					
			<div class="col-md-9">
			<div id="top-info">
			<h1>Los más influyentes de internet by @sclusib </h1>
			<h2>España, {list} 2016</h2>
			<p>Este es el ranking de los instagramers, youtubers, tuitstars, actores y cantantes que más lo están petando entre los jóvenes. No es un ranking por número de seguidores, influye más lo que están haciendo en el mes, sus interacciones, la calidad del contenido que suben y su proyección. ¿Crees que alguno merece otro puesto? Cuéntanoslo en nuestro twitter <a href="http://www.twitter.com/sclusib" target="_blank">@sclusib</a></p>
			</div>
				{top}								

			<div id="top-footer">
			Este es el listado de los influencers más importantes de españa para los jóvenes. De los chicos y chicas instagramers, youtubers, tuitstars, viners, actores y cantantes que más lo están petando. De  los reyes de las redes sociales, del social media y del influencer marketing. De los que más seguidores tienen en Instagram, Twitter, Snapchat, Vine o Ask.fm.  Pero no sólo influyen sus followers, también lo que están haciendo en el mes, sus interacciones, sus relaciones con las marcas, lo influyentes que son y su proyección. ¿Crees que alguno merece otro puesto? Cuéntanoslo en nuestro twitter <a href="http://www.twitter.com/sclusib" target="_blank">@sclusib</a>
			</div>
			</div>
			<div class="col-md-3">
				<div class="ad">
					
					<img src="{url}/img/ranking/ad_next.png" class="admockup">
					
					<br/>
					<br/>
					<a href="{_url_android}" onclick="trackOutboundLink('{_url_android}'); return false;" target="_blank"><img src="{url}img/download-android.png" width="120"></a>
					<a href="{_url_ios}" onclick="trackOutboundLink('{_url_ios}'); return false;" target="_blank"><img src="{url}img/download-ios.png" width="120"></a>
				</div>
			</div>		
		</div>													
	</div>
</div>  
{footer}		
</body>
</html>