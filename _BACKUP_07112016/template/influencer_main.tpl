{head}
<body>

<div class="main_section color_back influencer_main_bg">	
		<div class="header_transparent">	
		<div class="container">			
				{menu}		
				</div>
		</div>					
	<div class="container">									
		       <div class="space"></div>		        
		        <div class="row">
		        	
		        	<div class="col-md-12">
		        		<div class="p40"></div>
		        		<div id="influencer_intro">		        			
			        		<img src="{url}img/influencer/main/ganar_pasta.png"  class="influencer_responsive_small_img">
			        		<div class="p40"></div>
			        		<button class="button unete always">Únete a sclusib</button>
		        		</div>
		        	</div>
		        </div>	
		        <div class="p40"></div>
		        <div class="row">
		        	<div class="col-md-12 center">
		        		<div class="space"></div>
		        		<img src="{url}img/ic_bajar.png">		        		
		        		<div class="space"></div>
		        	</div>
		        </div>

	</div>
</div>	
<div class="main_section color_front">
	<div class="container">
		<div class="space"></div>
		<div class="row">		        
		   	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-push-6 col-md-push-6">
		   		<div class="influencer_right_text">
		   			<div class="p40"></div>
		      		<h1 class="influencer_responsive_main">Monetiza tus imágenes</h1>
		      		<h2 class="influencer_responsive_small influencer_responsive_h2">Monetiza tus imágenes</h2>
			        		<p class="roman sizeMain">Te pagamos por subir fotos en nuestra app. ¡Cuanta más gente las vea, más dinero ganas! Sencillo ¿no?</p>
	        		<div class="space"></div>
	        		<div class="row">
	        			<div class="col-xs-12 col-sm-12 mainDownload">
	        				<button class="button unete">Únete a sclusib</button>
	        			</div>
	        		</div>
        		</div>
        	</div>
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-pull-6 col-md-pull-6">
        		
        		<center><img src="{url}img/influencer/main/Mockup_influencer.png"  class="influencer_responsive_small_img"></center>
        	</div>
	    </div>	
	    <div class="space"></div>
	</div>
</div>	
<div class="main_section color_back">
	<div class="container">	
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-push-6 col-md-push-6">
				<h2 class="influencer_responsive_small influencer_responsive_h2">El club más exclusivo</h2>
        		<center><img src="{url}img/influencer/main/polaroid_main.png"  class="influencer_responsive_small_img"></center>
        	</div>		        
		   	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-pull-6 col-md-pull-6">
		   		<img src="{url}img/influencer/main/Polaroid_1.png" class="influencer_main_imgtop">
		   		<div class="influencer_left_text">
		      		<h1 class="influencer_responsive_main">El club más exclusivo</h1>
		      		
			        		<p class="roman sizeMain">En sclusib sólo pueden subir fotos jovenes con más de 10.000 seguidores que hemos estudiado y verificado. Una app donde instagramers, tuitstars, youtubers y viners crean una comunidad apasionante. Al compartir espacio con ellos podrás crecer en tus otras redes sociales.</p>
	        		<div class="space"></div>
	        		<div class="row">
	        			<div class="col-xs-12 col-sm-12">
	        				<button class="button unete">Únete a sclusib</button>
	        			</div>
	        		</div>
        		</div>
        	</div>        	
	    </div>
	    <div class="space"></div>		
	</div>
</div>	
<div class="main_section color_front">
	<div class="container">
	<div class="space"></div>
		<div class="row">					       
		   	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-push-6 col-md-push-6">
		   		<div class="influencer_right_text">
		      		<h1 class="influencer_responsive_main">Haz felices a tus fans</h1>
		      		<h2 class="influencer_responsive_small influencer_responsive_h2">Haz felices a tus fans</h2>
			        		<p class="roman sizeMain">Cada vez que usan sclusib dando like o comentando tus fotos, ganan puntos en tu ranking topfan. Sabrás quienes son los más activos y podrás premiar al número 1 con un follow, un saludo o una quedada.</p>
	        		<div class="space"></div>
	        		<div class="row">
	        			<div class="col-xs-12 col-sm-12">
	        				<button class="button unete">Únete a sclusib</button>
	        			</div>
	        		</div>
        		</div>
        	</div>  
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-lg-pull-6 col-md-pull-6">
        		
        		<center><img src="{url}img/influencer/main/polaroid_2.png" class="influencer_responsive_small_img"></center>
        	</div>      	
	    </div>
	    <div class="space"></div>			
	</div>
</div>
<div class="main_section color_back">
	<div class="container">
		<div class="row">			
			<div class="col-md-12 center">
				<h2 id="talentos">YA ESTÁN EN SCLUSIB</h2>				
			</div>
		</div>
		<div class="row">
			{influencers}
		</div>
		<div class="row">
			<div class="col-md-12 center">
				{see_all}
			</div>
		</div>

	</div>
	<div class="space"></div>
</div>
<div class="main_section color_front">
	<div class="container">
	<div class="space"></div>
		<div class="row">					       
		   	<div class="col-md-12">
		   		<div class="influencer_center_text">
		      		<h2 id="entrar">ÚNETE A SCLUSIB</h2>
			        		<div class="influencer_join">
			        		 <p class="influencer_left_text">Accediendo con tu Instagram podremos verificar tu número de seguidores</p>			
			        		 <div class="space"></div>	
		        				<img src="{url}img/influencer/main/Boton_Instagram.png" class="influencer_responsive_small_instagram join_sclusib">      		        				  		
			        		</div>	        		
			        		<div class="influencer_warning">
			        			<div class="influencer_warning_icon">
			        				<img src="{url}img/influencer/main/ic_informacion.png" style="cursor: pointer;">
			        			</div>
			        			<div class="influencer_warning_text">
			        				¡No entres en pánico! Con tu instagram nos darás acceso SOLAMENTE a tu información básica. No podemos ver o almacenar tu contraseña ni publicar en tu nombre.
			        			</div>
			        			<div class="clear"></div>
			        		</div>
        		</div>
        	</div>          	     
	    </div>
	    <div class="space"></div>			
	</div>
</div>
{footer}
		
</body>
</html>