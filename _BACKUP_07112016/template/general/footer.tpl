<div class="main_section color_footer relative">
	<div class="container">
		<div class="space p20"></div>
		<div class="row">
			<div class="col-xs-6 col-md-6">
				<img src="{url}img/logo-sclusib-inverted.png"><br/>
			   		
	    	</div>
	    	<div class="col-xs-6 col-md-6 right">
				<a href="https://www.instagram.com/sclusib/"  target="_blank"><img src="{url}img/social/instagram.png" width="30"></a>			
				<a href="https://twitter.com/sclusib"  target="_blank"><img src="{url}img/social/twitter.png" width="30"></a>				
	    	</div>
	    </div>
	    <div class="footerline"></div>
		<div class="row color_white roman size15">			
			<div class="col-md-8 col-md-push-4 col-xs-12 right footermenu">					    			  
			    <div class="row">			    			    					   		
			    	<div class="col-xs-6 col-md-4"><a href="{url}#talentos">Ya esta en sclusib</a></div>
			    	<div class="col-xs-6 col-md-2"><a href="{url}unete/">Eres influencer</a></div>
			    	<div class="col-xs-6 col-md-2"><a href="{url}top/">Top 100</a></div>
			    	<div class="col-xs-6 col-md-2"><a href="{url}blog/">Blog</a></div>	
			    	<div class="col-xs-6 col-md-2"><a href="{url}equipo/"><b>Equipo</b></a></div>			    		
			    </div>			    	
			</div>			
			<div class="col-md-4 col-md-pull-8 col-xs-12 footerLogo">
			    Consigue estar más cerca de tús ídolos.<br/>
			    ¡Ve contenido y gana premios!
			</div>
		</div>
		<br/><br/>
		<div class="row">
			<div class="col-md-12 center">
			 	<div class="footerAddress">Sclusib Studio S.L. Calle Roger de Lauria 19 Valencia (Spain)</div>	
			</div>
		</div>
			
	</div>
	<script type="text/javascript">
	
	
	$(function() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	</script>
	<script>
	$(window).scroll(function() {
	    if ($(this).scrollTop() > 1){  
	        $('.header').addClass("sticky");
	        $('.header_transparent').addClass("sticky");
	    }
	    else{
	        $('.header').removeClass("sticky");
	        $('.header_transparent').removeClass("sticky");
	    }
	});
	</script>
	
	
	<div class="box-send size14 roman">
		<img src="{url}img/ic_email.png" class="s-right"> <a href="mailto:hola@sclusib.com">¿Tienes alguna pregunta?</a>
	</div>
	
</div>