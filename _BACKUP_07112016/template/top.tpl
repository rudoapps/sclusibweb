<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>sclusib</title>
<!-- Bootstrap -->
<link rel="stylesheet" href="{url}bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{url}bootstrap/css/bootstrap-theme.min.css">
<script src="{url}bootstrap/js/bootstrap.min.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="{url}css/popup.css">
<link rel="stylesheet" href="{url}css/main.css">
<!-- FAVICON -->
<link rel="shortcut icon" type='image/x-icon' href="{url}favicon.ico"/>
<script src="{url}js/jquery-2.2.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
var change=0;
	
	$(function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $('ul#sortable').sortable({
        start: function(event, ui) {
            var start_pos = ui.item.index();
            ui.item.data('start_pos', start_pos);
        },
        update: function(event, ui) {
            var start_pos = ui.item.data('start_pos');
            var end_pos = ui.item.index();
            change = 1;
            $('#sortable li').each( function(e) {
              //alert("ANTES: "+$(this).find('.selectorID').val()+ " " +$(this).find('.selectorPOS').val());
              $(this).find('.selectorPOS').val($(this).index());
              //alert("AHORA: "+$(this).find('.selectorID').val()+ " " +$(this).find('.selectorPOS').val());
              //order.push( $(this).attr('id')  + '=' + ( $(this).index() + 1 ) );
             });
            //alert(start_pos + ' - ' + end_pos);
            //alert(ui.item.find('.selectorPOS').val());
            //ui.item.find('.selectorPOS').val(end_pos);
            //alert(ui.item.find('.selectorPOS').val());
        }
    });
	});

	$(document).ready(function(){	
		$( ".add" ).click(function() {
			$(".new-inf").fadeToggle();
		});
		$( ".change" ).click(function() {
			//alert(">>" + $(this).parent().find(".username").text());
			var data 	 = $(this).parent().parent().parent().find('img').attr("src");
			var username = $(this).parent().parent().find(".username").text();
			var id 		 = $(this).parent().parent().parent().find(".selectorID").val();
			var pos 	 = $(this).parent().parent().parent().find(".selectorPOS").val();
			var desc 	 = $(this).parent().parent().find(".desc").text();
				
			$(".img-desc img").attr("src",data);			
			$(".new-desc").find(".usernametop").html('');
			$(".new-desc").find(".usernametop").html(username);
			$(".new-desc").find(".itemID").val(id);
			$(".new-desc").find(".newDesc").val(desc);
			$(".new-desc").find(".itemPOS").val(pos);
			$(".new-desc").fadeToggle();
		});  
		$( "#accept-desc" ).click(function() {
			$( "#newDesc" ).submit();
		});
		$( "#accept" ).click(function() {
			$( "#newInfluencer" ).submit();
		}); 
		$( ".accept-list" ).click(function() {
			if(change==1){
				$(".new-list").fadeToggle();
				//$( "#newList" ).submit();
			}else{
				alert("No hay cambios");
			}
		}); 
		$(".accept-newlist").click(function(){
			$( "#newList" ).submit();
		});
		$('.delete').click(function(){
		    return confirm("Estas seguro que quieres borrar el registro?");
		});
	});
</script>
</head>
<body>
<div class="overlay"></div>
<div class="main_section color_front">	
	<div class="container">								
				<div class="space"></div>
				<div class="row">
		        	<div class="col-md-2">
		        		<a href="{url-website}"><img src="{url}img/logo-sclusib.png" class="logo"></a>
		        	</div>
		        	<div class="col-md-10">
		        		<ul class="nav nav-pills">
							<li><a href="#">Nuestros influencers</a></li>
							<li><a href="#">Preguntas frecuentes</a></li>
							<li class="active"><a href="#">¿Eres influencer?</a></li>											 
						</ul>		        		
		        	</div>	        	
		        </div>
	</div>
</div>
<div class="main_section color_front">
	<div class="container">	
		<div class="space"></div>
		<button type="button" class="btn btn-default  btn-sm fright add">
		 	<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> 
		</button>
		
		<div class="new-inf">
			<a href="#" class="add fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
			<p class="size12 roman">Escribe el nombre del influencer: </p>
			<form method="post" action="{url}top/newinfluencer" id="newInfluencer">
				<input type="text" name="newUsername">
				<button type="button" class="btn btn-success  btn-sm" id="accept">
					ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
				</button>	
			</form>
		</div>	

		<div class="new-desc">
			<a href="#" class="change fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
			<div class="top-card-img img-desc">
        			<img src="" width="20">
        	</div>
        	<div class="usernametop">{username}</div>
			<p class="size12 roman">Escribe la descripción: </p>
			<form method="post" action="{url}top/cambiar-descripcion" id="newDesc">
				<textarea name="newDesc" class="newDesc"></textarea>
				<input type="hidden" name="idDesc" class="itemID" value="">
				<input type="hidden" name="posDesc" class="itemPOS" value="">
				<button type="button" class="btn btn-success  btn-sm" id="accept-desc">
					ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
				</button>	
			</form>
		</div>	

		<button type="button" class="btn btn-default  btn-sm accept-list">
					REORGANIZAR    <span class="glyphicon glyphicon-sort" aria-hidden="true"></span> 
				</button>		
		
			<form method="post" action="{url}top/newlist" id="newList">
			<!-- FORMULARIO DE NOMBRE LISTA-->
					<div class="new-list">					
						<a href="#" class="accept-list fright"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
						<center>
							<button type="button" class="btn btn-default  btn-sm accept-newlist">
								NO QUIERO NOMBRE    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> 
							</button>	
						</center>
							<div class="hline"></div>
						<p class="size12 roman">Escribe el nombre de la lista: </p>						
							<input type="text" name="newListName">
							<button type="button" class="btn btn-success  btn-sm accept-newlist">
								ACEPTAR    <span class="glyphicon glyphicon-send" aria-hidden="true"></span> 
							</button>	
						
					</div>
			<div id="top">
			<ul id="sortable">
				
					{top}
					
			</ul>
			</div>				
			</form>
	</div>
</div>  
<div class="main_section color_footer relative">
	<div class="container">
		<div class="space p40"></div>
		<div class="row color_white roman size15">
			<div class="col-md-4">
			    <img src="{url}img/logo-sclusib-inverted.png">			   
			    <div class="space"></div>
			    <div class="line"></div>
			    	<div class="space"></div>
			    Consigue estar más cerca de tús ídolos.<br/>
			    ¡Ve contenido y gana premios!
			</div>
			<div class="col-md-8">
			    <div class="row">
			    	<div class="col-md-3 col-md-offset-1">		   	
			    		<ul class="nav-footer">
			    			<li><a href="#">Instagram</a></li>
			    			<li><a href="#">Twitter</a></li>
			    		</ul>
			    	</div>
			    	<div class="col-md-4">
			    		<ul class="nav-footer">
			    			<li><a href="#">Preguntas frecuentes</a></li>
			    			<li><a href="#">Nuestros talentos</a></li>
			    			<li><a href="#">Ayuda</a></li>
			    			<li><a href="#">Acerca de sclusib</a></li>
			    		</ul>
			    	</div>
			    	<div class="col-md-4">
			    		<ul class="nav-footer">
			    			<li><a href="#">Contacto</a></li>
			    			<li><a href="#">Cookies</a></li>
			    			<li><a href="#">Política de Privacidad</a></li>
			    			<li><a href="#">Términos de uso</a></li>
			    		</ul>
			    	</div>
			    </div>
			</div>

		</div>
		<div class="space p40"></div>			
	</div>
	<div class="box-send size14 roman">
		<img src="{url}img/ic_email.png" class="s-right"> <a href="#">¿Tienes alguna pregunta?</a>
	</div>
</div>
		
</body>
</html>