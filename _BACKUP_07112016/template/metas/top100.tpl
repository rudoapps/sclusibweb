<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sclusib">
<meta name="twitter:creator" content="@sclusib">
<meta name="twitter:title" content="Los más influyentes de internet by @sclusib">
<meta name="twitter:description" content="Este es el ranking de los instagramers, youtubers, tuitstars, actores y cantantes que más lo están petando entre los jóvenes. No es un ranking por número de seguidores, influye más lo que están haciendo en el mes, sus interacciones, la calidad del contenido que suben y su proyección. ¿Crees que alguno merece otro puesto? Cuéntanoslo en nuestro twitter @sclusib">
<meta name="twitter:image" content="http://www.sclusib.com/img/tweet/top100.jpg?{random}">