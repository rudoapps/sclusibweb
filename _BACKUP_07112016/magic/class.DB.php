<?php
class DB{

	var $connection;
	var $server;
	var $username;
	var $password;
	var $database;
	
	function DB($server,$username,$password,$database){		
		$this->server=$server;
		$this->username=$username;
		$this->password=$password;
		$this->database=$database;		
	}
	
	function open(){
		$this->connection = new mysqli($this->server,
									  $this->username,
									  $this->password,
									  $this->database
									  );
		if ($this->connection->connect_errno) { echo "ERROR al conectar";}
	}
	
	function query($sql){
		return $this->connection->query($sql);
	}

	function error(){
    	echo "<b>Error:</b> " . $this->connection->error;
	}

	function close(){
		$this->connection->close();
	}
}
?>