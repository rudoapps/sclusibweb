<?php
include_once("config.php");
include_once("class.DB.php");

class instagram{	
	
	var $JSON;
	var $user;
	var $obj;
	var $rawPage;
	var $DB;
	public $ok , $isPrivate;


	function instagram($user){
		$this->user 	 = $user;
		$this->isPrivate = false;
		try{
			$this->JSON = @file_get_contents('https://www.instagram.com/'.$user.'/media/'); 
			$this->JSON = utf8_encode($this->JSON); 
			$this->obj  = json_decode($this->JSON, true);
			if($this->JSON===FALSE){
				$this->ok 	= false;
			}
			else{
				$this->ok 	= "OK";		
				//echo strlen($this->JSON);		
				if(strlen($this->JSON)<40){
					$this->isPrivate = true;
				}				
			}
		}catch (Exception $e){
			$this->ok   = false;
		}
		try{
			$this->rawPage  = @file_get_contents('https://www.instagram.com/'.$user);
			if($this->rawPage===FALSE)
				$this->ok 	= false;
			else
				$this->ok 	= "OK";
		}catch (Exception $e){
			$this->ok   = false;
		}

		
	}
	
	function getPicture(){
		if($this->obj['items'][0]['caption']['from']['profile_picture']!=null){
			return $this->obj['items'][0]['caption']['from']['profile_picture'];
		}else{
			$this->ok=false;
			return null;
		}

	}

	function getId(){		
		if($this->obj['items'][0]['caption']['from']['id']!=null){
			return $this->obj['items'][0]['caption']['from']['id'];
		}else{
			$this->ok=false;
			return null;
		}
	}


	function getFollowers(){
		preg_match('/\"followed_by\"\: \{\"count\"\: ([0-9]+)/', $this->rawPage, $m);
		return  intval($m[1]);
	}

	function getBio(){
		//preg_match('/\"biography\"\:\"([0-9]+)/', $this->rawPage, $m);
		return  "";
	}

	function cronData(){						
		
	}
}