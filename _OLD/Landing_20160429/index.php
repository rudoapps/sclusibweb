<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<title>sclusib</title>
<meta name="description" content="" />
<link rel="stylesheet" type="text/css" href="css/estilo.css" />
<script type="text/javascript" src="js/codigo.js"></script>
<meta name="Author" content="Marcos Plazas" />
<meta name="Keywords" content="red social, " />
<meta name="Robots" content="index, follow" />
<meta name="Googlebot" content="index, follow" />
<meta name="Copyright" content="" />
<meta name="Revisit" content="" />
<meta name="Distribution" content="Global" />
<meta http-equiv='Pragma' content='no-cache' />
<meta property="fb:app_id" content="" />
<meta property="fb:admins" content="" />
<meta property="og:type" content="" />
<meta property="og:title" content="" />
<meta property="og:image" content="" />
<meta property="og:description" content="" />
<meta property="og:url" content="">
<meta property="og:locale" content="es_ES" />
<meta name="twitter:card" content="app">
<meta name="twitter:site" content="@sclusib">
<meta name="twitter:description" content="Con sclusib tendrás acceso a fotos exclusivas de tus famosos favoritos. Cada vez que interactúes con ellos conseguirás puntos en nuestro ranking Top Fan y podrás ganar premios por todo el apoyo que les das.">
<meta name="twitter:app:country" content="ES">
<meta name="twitter:app:name:iphone" content="sclusib">
<meta name="twitter:app:id:iphone" content="1043593798">
<meta name="twitter:app:url:iphone" content="https://itunes.apple.com/us/app/sclusib/id1043593798">
<meta name="twitter:app:name:googleplay" content="sclusib">
<meta name="twitter:app:id:googleplay" content="com.sclusib.app">
<meta name="twitter:app:url:googleplay" content="https://play.google.com/store/apps/details?id=com.sclusib.app">
</head>

<body>

<header>
	<img class="logo" src="img/dp_logo_sclusib.png">
</header>

<content>
	<h3>Con sclusib tendrás acceso a fotos exclusivas de tus famosos favoritos. Cada vez que interactúes con ellos conseguirás puntos en nuestro ranking Top Fan y podrás ganar premios por todo el apoyo que les das. </h3>

	<div class="appstores">
		<a href="https://itunes.apple.com/es/app/sclusib/id1043593798" target="_blank"><img src="img/disp_ic_appstore.png"></a>
		<a href="https://play.google.com/store/apps/details?id=com.sclusib.app" target="_blank"><img src="img/disp_ic_googleplay.png"></a>
	</div>

	<p class="textopeque">Tienes más de 25.000 usuarios en twitter o Instagram? Escríbenos a <a href="mailto:hola@sclusib.com"><strong class="email">hola@sclusib.com</strong></a> con la dirección de tus redes sociales y te enviaremos más información.</p>
	<br/><br/><br/>
	<p class="textopeque"><b>sclusib · C/Roger de Lauria  28 · 46002 · Valencia · <a href=mailto:hola@sclusib.com">hola@sclusib.com</a></b></p>
</content>

<footer>	
	
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61907046-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
