<?php
include_once('phpmailer/class.smtp.php');
include_once('phpmailer/class.phpmailer.php');

class EMAIL {
  	
	public $mail;
	
	
	public function __construct(){		
		$this->mail             = new PHPMailer();		
		$this->mail->Host       = _MAIL_SMTP;
		$this->mail->IsSMTP(); 
		$this->mail->SMTPDebug  = _MAIL_DEBUG;
		$this->mail->SMTPAuth   = _MAIL_SMTPAUTH;
		$this->mail->SMTPSecure = _MAIL_SMTPSECURE;
		$this->mail->Port       = _MAIL_SMTPPORT;
		$this->mail->Username   = _MAIL_USERNAME;
		$this->mail->Password   = _MAIL_PASSWORD;
		$this->mail->From	 	= _MAIL_ADDRESS;
		$this->mail->FromName 	= _MAIL_ADDRESS_DESC;				 	
	}	      
    
    public function sendEmail($email) {       							    	     	  
    	$this->mail->setLanguage('es', '/phpmailer/language');
    	$this->mail->isHTML(true);
		$this->mail->Subject    = "Bienvenido a sclusib";	
		$body=file_get_contents("http://www.sclusib.com/include/class/phpmailer/plantillas/registro.html");			
		//$this->mail->AltBody    = "Para poder ver el mensaje accede con un gestor de correo "; // optional, comment out and test				 						
		$this->mail->Body    	= $body;	
		$this->mail->AddAddress($email);			
		if(!$this->mail->send()) {			
			return false;
		} else {
			return true;
		}
    }
}
?>