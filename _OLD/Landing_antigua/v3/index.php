<!DOCTYPE html>
<!--
Versión 1.0  
-->
<html>

<head>
	<title>Sclusib</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<script type="text/javascript" src="js/lib/jquery.min.js"></script>
	<script type="text/javascript" src="js/lib/highlight.pack.js"></script>
	<script type="text/javascript" src="js/lib/modernizr.custom.min.js"></script>
	<script type="text/javascript" src="js/examples.js"></script>
	
	<script type="text/javascript" src="js/lib/greensock/TweenMax.min.js"></script>
	<script type="text/javascript" src="js/scrollmagic/uncompressed/ScrollMagic.js"></script>
	<script type="text/javascript" src="js/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
	<script type="text/javascript" src="js/scrollmagic/uncompressed/plugins/debug.addIndicators.js"></script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300|Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/normalizar.css" />	
	<style type="text/css">
	.parallaxParent {
		height: 100vh;
		overflow: hidden;
	}
	.parallaxParent > * {
		height: 200%;
		position: relative;
		top: -100%;
	}
	
</style>	
</head>
<body>

<div class="spacer s0"></div>
<div id="parallax1" class="parallaxParent">
	<div style="background-image: url(img/bg1.jpg);">
		<img src="img/iPhone1.png">
	</div>
</div>
<div class="spacer s1">
	<div class="box2 blue">
		<p>Content 1</p>
		<a href="#" class="viewsource">view source</a>
	</div>
</div>
<div class="spacer s0"></div>
<div id="parallax2" class="parallaxParent">
	<div style="background-image: url(img/bg2.jpg);"></div>
</div>
<div class="spacer s1">
	<div class="box2 blue">
		<p>Content 2</p>
		<a href="#" class="viewsource">view source</a>
	</div>
</div>
<div class="spacer s0"></div>
<div id="parallax3" class="parallaxParent">
	<div style="background-image: url(img/bg3.jpg);"></div>
</div>
<div class="spacer s2"></div>
<script>
	// init controller
	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

	// build scenes
	new ScrollMagic.Scene({triggerElement: "#parallax1"})
					.setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	new ScrollMagic.Scene({triggerElement: "#parallax2"})
					.setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);

	new ScrollMagic.Scene({triggerElement: "#parallax3"})
					.setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})
					.addIndicators()
					.addTo(controller);
</script>
</body>
</html>