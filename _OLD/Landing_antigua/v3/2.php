<!DOCTYPE html>
<!--
Versión 1.0  
-->
<html>

<head>
	<title>Sclusib</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300|Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css" />
	<link rel="stylesheet" media="screen and (min-width: 600px)" href="css/large.css" />
	<link rel="stylesheet" href="css/1140.css" />	
		
</head>
<body>
	<div id="primeraEtapa">
				
		<div id="contenido-wrap">
			<div id="logo">
				<img src="img/logo.png">
			</div>
			<h1>CONECTA CON <br/>TUS SEGUIDORES</h1>
			<h2>Comparte contenido exclusivo y gana dinero con cada visualización</h2>
		</div>
		<div id="mano-wrap">			
			<img src="img/mockup-profile-dcha.png">
		</div>
			
	</div>
	<div id="segundaEtapa">
			<div id="mockup-segundaEtapa" data-200="transform:translate3d(0,60%,0);" data-500="transform:translate3d(0,5%,0);">
				<img src="img/mockup-2.png">
			</div>
			<div id="contenido-segundaEtapa"  data-200="transform:translate3d(60%,0,0);" data-500="transform:translate3d(0%,0,0);">
				<h1>QUIERES UTILIZAR SCLUSIB?</h1>
				<h2>Una vez hayamos valorado tu influencia te daremos acceso privado y podrás empezar a compartir las experiencias, fotos y videos cortos que nunca hayas enseñado antes. Anima a tus seguidores en otras redes sociales a que te sigan también en sclusib.</h2>
			</div>
	</div>
	<div id="terceraEtapa">
			<div id="contenido-terceraEtapa"  data-top="transform:translate3d(0%,0,0);" data-0-center-center="transform:translate3d(-60%,0,0);">
				
				<h2>Estamos creando la aplicación más robusta y revolucionaria para descubrir contenido. Proporcionamos a tus fans una app dinámica y ágil, una comunidad donde pueden descubrir fotos y videos exclusivos que no han visto en ninguna red social. En función de su interacción contigo podrán escalar posiciones en nuestro ranking Top Fan ¿Llegarán a ser tu fan número uno?</h2>
			</div>
			<div id="mockup-terceraEtapa" data--200-top="transform:translate3d(0,0%,0);" data-0-center-center="transform:translate3d(0,60%,0);">
				<img src="img/metricas.png">
			</div>			
			<div class="clear"></div>
			
			
	</div>	
	<div id="cuartaEtapa">
	</div>
	
	
	<script type="text/javascript" src="js/skrollr.js"></script>
	<script type="text/javascript">
	skrollr.init({
		forceHeight: false,				
	});
	</script>	
</body>
</html>