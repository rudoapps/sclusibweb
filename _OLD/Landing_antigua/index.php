<!DOCTYPE html>
<!--
Versión 1.0  
-->
<html>

<head>
	<title>Sclusib</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/1140.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="js/zoom.js"></script>
	<link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css" />	
	<link rel="stylesheet" media="screen and (min-width: 600px)" href="css/large.css" />
	<script>
	$(document).ready(function() {
	    $(".notificacion").delay(3000).fadeOut(2000);
	}); // <---
	</script>	
</head>
<body>

	<div id="primeraEtapa">
		<div id="primerMovil">
		</div>
		<div class="contenedor">
			<?php
			
			switch($_GET['go']){
				case 'success':
					
			?>
			<div class="notificacion">
				<b>Gracias por tu interés</b><br/>
				<p>Nos pondremos en contacto contigo cuando la app de sclusib este disponible</p>
			</div>		
			<?php
				break;
				case 'registered':
			?>
			<div class="notificacion">
				<b>Tu email ya esta registrado</b><br/>
				<p>No te preocupes, te mantedremos informado ;)</p>			
			</div>		
			<?php
			}
			?>	
			<div id="logo">
				<img src="img/logo.png">
			</div>	
			<div id="contenido-primeraEtapa">
				
				<h1>CONECTA CON <br/>TUS SEGUIDORES</h1>
				<h2>Comparte contenido exclusivo y gana dinero con cada visualización.</h2>
				
				<h3>sclusib es la plataforma que va a cambiar el modo en el que te relacionas con tus fans. Una aplicación totalmente exclusiva para personas con más de 25.000 seguidores en Twitter o Instagram.</h3>
				<div id="icono-primeraEtapa">
					<img src="img/monetiza.png">
				</div>
			</div>
			<div id="mockup-primeraEtapaMovil">			
				<img src="img/i1.png">
			</div>
			<div id="mockup-primeraEtapa">			
				<img src="img/i1.png">
			</div>
			<div class="clear"></div>
		</div>	
	</div>
	<div id="primerParallax">
		<div class="boton">
			FUNCIONALIDADES
		</div>
	</div>
	<div id="segundaEtapa">
		<div class="contenedor">		
			<div id="contenido-segundaEtapa" >
				<h1>COMPARTE<br/>CONTENIDO</h1>
				<h3>Una vez hayamos valorado tu influencia te daremos acceso privado y podrás empezar a compartir las experiencias, fotos y videos cortos que nunca hayas enseñado antes.
				Anima a tus seguidores en otras redes sociales a que te sigan también en sclusib.
				</h3>
			</div>	
			<div id="icono-segundaEtapa">
					<img src="img/acceso-exclusivo.png">
				</div>
			<div id="mockup-segundaEtapa" >
				<img src="img/i2.png">
			</div>			
			<div class="clear"></div>
		</div>
	</div>
	<div id="terceraEtapa">
		<div class="contenedor">	
			<div id="contenido-terceraEtapa">				
				<h1>MONITORIZA LOS<br/>RESULTADOS</h1>
				<h3>Con nuestro programa de reparto de ingresos publicitarios ganarás desde el primer día: Cuantos más seguidores, más visualizaciones y más clicks en los anuncios y por tanto, más dinero para ti.  Podrás ver en tiempo real el alcance de tus publicaciones, las interacciones con los fans, y lo más importante, una estimación de los ingresos que estás logrando. </h3>	
						</div>
			<div id="mockup-terceraEtapa">
				<img src="img/i3.png">
			</div>			
			<div class="clear"></div>
		</div>			
	</div>	
	<div id="cuartaEtapa">
		<div class="contenedor">	
			<div id="mockup-cuartaEtapa" >
				<img src="img/i4.png">
			</div>
			<div id="mockup-cuartaEtapaMovil">			
				<img src="img/i4m.png">
			</div>
			<div id="contenido-cuartaEtapa" >
				<h1>LUCHA POR SER<br/>EL FAN Nº1</h1>
				<h3>Estamos creando la aplicación más robusta y revolucionaria para descubrir contenido.
					Proporcionamos a tus fans una app dinámica y ágil, una comunidad donde pueden descubrir fotos y videos exclusivos, que no se han visto en ninguna red social.
					 En función de su interacción contigo podrán escalar posiciones en nuestro ranking Top Fan ¿Llegarán a ser tu fan número uno?
				</h3>
			</div>
		<div class="clear"></div>
		</div>	
	</div>
	<div id="primerParallax">
		<div class="boton">
			NUESTRO EQUIPO
		</div>
	</div>
	<div id="quintaEtapa">
		<div class="contenedor">
			<div id="equipo-quintaEtapa">
				<div class="miembro">
					<a href="https://es.linkedin.com/in/richardmorla/es" class="equipo"><img src="img/foto-Richard.png"></a>
					<div class="miembro-info">
						<h1>Richard Morla</h1>
						<h2>CEO</h2>
					</div>
				</div>
				<div class="miembro">
					<a href="https://es.linkedin.com/pub/marcos-plazas-sánchez/96/b53/608"><img src="img/foto-Marcos.png"></a>
					<div class="miembro-info">
						<h1>Marcos Plazas</h1>
						<h2>CDO</h2>
					</div>
				</div>
				<div class="miembro">
					
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div id="sextaEtapa">
		<div class="contenedor">
			<div id="contenido-sextaEtapa">
				
				<h1>¿QUIERES<br/> SABER MÁS?</h1><br/>
				<h4>Déjanos tu mail si tienes más de 25.000 seguidores en Twitter o Instagram y quieres que te informemos cuando la app de sclusib esté disponible.</h4>	
			<br/><br/>
			<form method="post" action="control.php?do=email">
				<input type="email" placeholder="escribe tu email" name="email" id="email">
				<input type="submit" value="Enviar">
			</form>
			<h4>
				<div id="logo-pie">
					<img src="img/logo.png">
				</div>
				Calle Almirante Roger de Lauria 28 pta.2 Valencia<br/>
				<a href="mailto:hola@sclusib.com">hola@sclusib.com</a><br/><br/>
				<a href="https://instagram.com/sclusib"><img src="img/instagram.png" class="social"></a>
				<a href="https://www.facebook.com/sclusib"><img src="img/facebook.png" class="social"></a>
				<a href="https://twitter.com/sclusib"><img src="img/twitter.png" class="social"></a>
				<div class="clear"></div>
			</h4>
						</div>
			<div id="mapa-sextaEtapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1540.0054381457783!2d-0.3726702000000213!3d39.469083000000026!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6048b48b3b69c9%3A0x77e763bd6f9ab5e4!2sCalle+del+Almirante+Roger+de+Lauria%2C+28%2C+46002+Valencia!5e0!3m2!1sen!2ses!4v1429857536241" width="100%" height="450" frameborder="0" style="border: 1px solid #DDD; pointer-events:none;"></iframe>
			</div>			
			<div class="clear"></div>
			
		</div>
	</div>	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61907046-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>