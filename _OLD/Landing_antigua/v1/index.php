<!DOCTYPE html>
<!--
Versión 1.0  
-->
<html>

<head>
	<title>Sclusib</title>	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300|Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css" />
	<link rel="stylesheet" media="screen and (min-width: 600px)" href="css/large.css" />
	">	
</head>
<body>
	<div id="top">
		<div id="informacion">
			Calle Almirante Roger de Lauria 28 pta. 2. Valencia<br/>
			<b>hola@sclusib.com</b>
		</div>
		<a href="/">
		<div id="logo">
		</div>
		</a>
	</div>
	
	<div id="contenedor">
		<?php
		switch($_GET['go']){
			case 'success':
		?>
		<h1>Gracias por tu interés</h1>
		<h2>Nos pondremos en contacto contigo cuando la app de sclusib este disponible</h2>
		<?php
			break;
			case 'registered':
		?>
		<h1>Tu email ya esta registrado</h1>
		<?php
			break;
			default:
		?>
		<h1>Conecta con tus seguidores</h1>
		<h2>Comparte contenido exclusivo y gana dinero con cada visualización</h2>
		<div id="contacto">
			<b>¿Tienes más de 25.000 seguidores <br/>
			en Twitter o Instagram?</b>
			<p>Déjanos tu email y te informaremos cuando <br/>
			la app de sclusib esté disponible.</p>
			<form method="post" action="control.php?do=email">
				<input type="email" placeholder="escribe tu email" name="email" id="email">
				<input type="submit" value="Aceptar">
			</form>
		</div>
		<?php
			break;
		}
		?>
	</div>
</body>
</html>