<!DOCTYPE html>
<!--
Versión 1.0  
-->
<html>

<head>
	<title>Sclusib</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300|Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/1140.css" />	
	<link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css" />
	<link rel="stylesheet" media="screen and (min-width: 600px)" href="css/large.css" />	
</head>
<body>
	<div id="primeraEtapa">
				
		<div id="contenido-wrap">
			<div id="logo">
				<img src="img/logo.png">
			</div>
			<h1>CONECTA CON <br/>TUS SEGUIDORES</h1>
			<h2>Comparte contenido exclusivo y gana dinero con cada visualización.	<br/>
					sclusib es la plataforma que va a cambiar el modo en el que te relacionas con tus fans. Una aplicación totalmente exclusiva para personas con más de 25.000 seguidores en Twitter o Instagram.</h2>
		</div>
		<div id="mano-wrap">			
			<img src="img/i1.png">
		</div>
			
	</div>
	<div id="segundaEtapa">
			<div id="mockup-segundaEtapa" >
				<img src="img/i2.png">
			</div>
			<div id="contenido-segundaEtapa" >
				<h1 style="text-align:right;">COMPARTE<br/>CONTENIDO</h1>
				<h2 style="text-align:right;">Una vez hayamos valorado tu influencia te daremos acceso privado y podrás empezar a compartir las experiencias, fotos y videos cortos que nunca hayas enseñado antes.
				</h2>
			</div>
	</div>
	<div id="terceraEtapa">
			<div id="contenido-terceraEtapa">
				
				<h1>MONITORIZA LOS<br/>RESULTADOS</h1>
				<h2>Con nuestro programa de reparto de ingresos publicitarios ganarás desde el primer día: Cuantos más seguidores, más visualizaciones y más clicks en los anuncios y por tanto, más dinero para tí</h2>	
						</div>
			<div id="mockup-terceraEtapa">
				<img src="img/i3.png">
			</div>			
			<div class="clear"></div>
			
			
	</div>	
	<div id="cuartaEtapa">
			<div id="mockup-cuartaEtapa" >
				<img src="img/i4.png">
			</div>
			<div id="contenido-cuartaEtapa" >
				<h1 style="text-align:right;">LUCHA POR SER<br/>EL FAN Nº1</h1>
				<h2 style="text-align:right;">Estamos creando la aplicación más robusta y revolucionaria para descubrir contenido.
					Proporcionamos a tus fans una app dinámica y ágil, una comunidad donde pueden descubrir fotos y videos exclusivos, que no se han visto en ninguna red social
				</h2>
			</div>
	</div>
	<div id="quintaEtapa">
			<div id="contenido-quintaEtapa">
				
				<h1>¿ERES INFLUENCER?</h1>
				<h2>Déjanos tu mail si tienes más de 25.000 seguidores en Twitter o Instagram y quieres que te informemos cuando la app de sclusib esté disponible.</h2>	
			<br/><br/>
			<form method="post" action="control.php?do=email">
				<input type="email" placeholder="escribe tu email" name="email" id="email">
				<input type="submit" value="Aceptar">
			</form>
			<p style="padding-top:20px; font-weight:bold;">
				<div id="logo">
					<img src="img/logo.png">
				</div>
				Calle Almirante Roger de Lauria 28 pta.2 Valencia<br/>
				<a href="mailto:hola@sclusib.com">hola@sclusib.com</a>
			</p>
						</div>
			<div id="mapa-quintaEtapa">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3080.014063340526!2d-0.37266099999999996!3d39.469010999999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd6048b5de8c47fd%3A0xfed2fcac053e291b!2sDemium+Startups!5e0!3m2!1sen!2ses!4v1429811497616" width="100%" height="450" frameborder="0" style="border:0"></iframe>
			</div>			
			<div class="clear"></div>
			
			
	</div>	
</body>
</html>